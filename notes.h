//
// Created by Nulano on 17. 6. 2019.
//

#ifndef PORTAL2TIMER_NOTES_H
#define PORTAL2TIMER_NOTES_H

#include "pch.h"
#include "game.h"

const LPCWSTR cszSettingNotesOpen   = L"Portal2Timer.notes.window_open";
const LPCWSTR cszSettingNotesX      = L"Portal2Timer.notes.x";
const LPCWSTR cszSettingNotesY      = L"Portal2Timer.notes.y";
const LPCWSTR cszSettingNotesH      = L"Portal2Timer.notes.h";
const LPCWSTR cszSettingNotesFile   = L"Portal2Timer.notes.file";

ATOM RegisterNotesWindowClass();

void ScrollNotes(Map * map);

HWND OpenNotesWindow(HWND parent, bool topmost);
HWND GetNotesWindow();

#endif //PORTAL2TIMER_NOTES_H
