//
// Created by Nulano on 5. 2. 2018.
//

#ifndef PORTAL2TIMER_VERSION_H
#define PORTAL2TIMER_VERSION_H

#define TEXTW__(text) L##text
#define TEXTW(text) TEXTW__(text)

#define VERSION_NUM 0,1,0,0
#define VERSION_STR "0.1"
#define VERSION_WSTR TEXTW(VERSION_STR)

#endif //PORTAL2TIMER_VERSION_H
