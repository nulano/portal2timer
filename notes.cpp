
#include "pch.h"

#include "notes.h"
#include "timer.h"

#include "ui.h"

#define IDC_NOTES_OPEN      0x4100
#define IDC_NOTES_SAVE      0x4102
#define IDC_NOTES_GENERATE  0x4104

const LPCTSTR clpszNotesClassName = TEXT("NulanoPortal2TimerNotes");

//FIXME duplicated variables
static const int szConfig = 16;

//                                                size            weight I  U  S
static const HFONT fntConfig       = CreateFont(szConfig,  0, 0, 0, 400, 0, 0, 0, 1, 0, 0, 0, 0, L"Segoe UI");
static const HFONT fntConfigPath   = CreateFont(szConfig,  0, 0, 0, 400, 1, 0, 0, 1, 0, 0, 0, 0, L"Segoe UI");

static auto root = Window({0, 0, 400, 200});

static auto phNotes = PathHeader<3>({0, root.top, root.right, root.top + 20},
                                    fntConfig, fntConfigPath, "Notes File:",
                                    {{{L"Generate Blank Notes File", icoReload, IDC_NOTES_GENERATE},
                                      {L"Save Notes File...", icoSave, IDC_NOTES_SAVE},
                                      {L"Open Notes File...", icoOpen, IDC_NOTES_OPEN}}});

static auto reNotes = RichEdit({0, phNotes.bottom, root.right, root.bottom}, clGray4,
        ES_MULTILINE | ES_WANTRETURN | WS_VSCROLL | ES_AUTOVSCROLL);

static auto __attribute((__unused__)) __bottom = root.bottom = reNotes.bottom;

void ScrollNotes(Map * map) {
    if (!root.hwnd)
        return;

    wchar_t file[256]; file[255] = 0;
    mbstowcs(file, map->file, 255);

    FINDTEXTEXW ftex = {};
    ftex.lpstrText  = file;
    ftex.chrg.cpMin = 0;
    GETTEXTLENGTHEX gtl = {};
    ftex.chrg.cpMax = (LONG) (LONG_PTR) SendMessage(reNotes, EM_GETTEXTLENGTHEX, (WPARAM) (DWORD_PTR) &gtl, 0);

    if (SendMessage(reNotes, EM_FINDTEXTEXW, (WPARAM) FR_DOWN, (LPARAM) &ftex) >= 0) {
        POINTL pointl = {};
        SendMessage(reNotes, EM_POSFROMCHAR, (WPARAM) (DWORD_PTR) &pointl, ftex.chrgText.cpMin);
        POINT point = {};
        point.x = 0; point.y = pointl.y - 10;
        SendMessage(reNotes, EM_SETSCROLLPOS, 0, (LPARAM) (LONG_PTR) &point);
    }
}

LRESULT CALLBACK NotesWndProc(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam) {
    auto & settings = SettingsGet();
    switch (umsg) {
        case WM_CREATE: {
            root.hwnd = hwnd;
            for (Window * wnd : Window::instances)
                if (!wnd->Create(hwnd))
                    return -1;

            // TODO load notes

            return DefWindowProc(hwnd, umsg, wparam, lparam);
        }
        case WM_SIZING: {
            //int type = wparam;
            RECT * rect = (RECT*) lparam;
            RECT prev = {}; GetWindowRect(hwnd, &prev);
            rect->left = prev.left;
            rect->right = prev.right;
            // TODO min-height
            return TRUE;
        }
        case WM_SIZE: {
            UINT width = LOWORD(lparam), height = HIWORD(lparam);
            MoveWindow(reNotes, reNotes.left, reNotes.top, width, height - reNotes.top, TRUE);
            return 0;
        }
        case WM_COMMAND: {
            if (HIWORD(wparam) == BN_CLICKED) {
                switch (LOWORD(wparam)) {
                    case IDC_NOTES_GENERATE: {
                        // TODO overwrite warning

                        MapId id = {0, 0};

                        EDITSTREAM es = {};
                        es.dwError = S_OK;
                        es.dwCookie = (DWORD_PTR) &id;
                        es.pfnCallback = [](DWORD_PTR dwCookie, LPBYTE lpBuff, LONG cb, PLONG pcb) -> DWORD {
                            MapId & mapId = *((MapId*) dwCookie);

                            if (mapId.chapter >= 1000) {
                                *pcb = 0;
                                return 0;
                            }

                            Game * game = GetCurrentGame();
                            Map * map = game->getMap(mapId);

                            string line = string(map->file) + " (" + string(map->name) + ")\r\n\r\n";
                            if (line.size() > cb)
                                return 1; // BUFFER TOO SMALL // TODO show error
                            strcpy((LPSTR) lpBuff, line.c_str());
                            *pcb = line.size();

                            if (!game->advance(mapId))
                                mapId = {1000, 1000};

                            return 0;
                        };
                        SendMessage(reNotes, EM_STREAMIN, SF_TEXT, (LPARAM) &es);

                        return 0;
                    }
                    case IDC_NOTES_OPEN: {
                        // TODO overwrite warning

                        wchar_t path[4098];
                        wcscpy(path, settings.get<wstring>(cszSettingNotesFile, L"").c_str());

                        OPENFILENAMEW ofn = {};
                        ofn.lStructSize = sizeof(ofn);
                        ofn.hwndOwner = root;
                        ofn.hInstance = hinstance;
                        ofn.lpstrFilter = L"Rich Text File (*.rtf)\0"   L"*.rtf\0"
                                          L"Plain Text File (*.txt)\0"  L"*.txt\0"
                                          L"All Files\0"                L"*.*\0";
                        ofn.nFilterIndex = 1;
                        ofn.lpstrFile = path;
                        ofn.nMaxFile = sizeof(path) / sizeof(path[0]);
                        ofn.lpstrInitialDir = NULL; // TODO
                        ofn.lpstrTitle = L"Select Run Notes";
                        ofn.Flags = OFN_FILEMUSTEXIST;

                        if (!GetOpenFileNameW(&ofn)) {
                            //TODO switch(CommDlgExtendedError()) { ... }
                            return 0;
                        }

                        HANDLE hFile = CreateFile(path, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);

                        if (hFile != INVALID_HANDLE_VALUE) {
                            EDITSTREAM es = {};
                            es.dwError = S_OK;
                            es.dwCookie = (DWORD_PTR) hFile;
                            es.pfnCallback = [](DWORD_PTR dwCookie, LPBYTE lpBuff, LONG cb, PLONG pcb) -> DWORD {
                                return ReadFile((HANDLE) dwCookie, lpBuff, cb, (DWORD*) pcb, NULL) ? (DWORD) 0 : GetLastError();
                            };
                            SendMessage(reNotes, EM_STREAMIN, ofn.nFilterIndex == 1 ? SF_RTF : SF_TEXT, (LPARAM) &es);
                            CloseHandle(hFile);

                            if (es.dwError == S_OK) {
                                SetWindowTextW(phNotes.Path(), path);
                                settings.put<wstring>(cszSettingGameDir, wstring(path));
                            }
                            // TODO else switch (es.dwError) {...}
                        }

                        return 0;
                    }
                    case IDC_NOTES_SAVE: {
                        //FIXME
                        return 0;
                    }
                }
            }
            return 0;
        }
        case WM_NCDESTROY: {
            root.hwnd = NULL;
            for (Window * wnd : Window::instances)
                wnd->hwnd = NULL;

            return DefWindowProc(hwnd, umsg, wparam, lparam);
        }
        case WM_DESTROY: {
            RECT pos = {};
            if (GetWindowRect(root, &pos)/* && AdjustWindowRect(&pos, (DWORD) GetWindowLongPtr(root, GWL_STYLE), TRUE)*/) {
                settings.put(cszSettingNotesX, pos.left);
                settings.put(cszSettingNotesY, pos.top);
                settings.put(cszSettingNotesH, pos.bottom - pos.top);
            }
            return DefWindowProc(hwnd, umsg, wparam, lparam);
        }
        case WM_CLOSE: {
            // TODO save notes confirmation?

            settings.put(cszSettingNotesOpen, false);

            DestroyWindow(root);
            return 0;
        }
        default:
            return RootWndProc(hwnd, umsg, wparam, lparam);
    }
}

ATOM RegisterNotesWindowClass() {
    WNDCLASS wndclass = {};
//    wndclassex.cbSize = sizeof(wndclass);
    wndclass.style = CS_HREDRAW | CS_VREDRAW;
    wndclass.lpfnWndProc = NotesWndProc;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 0;
    wndclass.hInstance = hinstance;
    wndclass.hIcon = LoadIcon(hinstance, MAKEINTRESOURCE(IDI_ICONMAIN));
    //wndclassex.hIconSm = wndclass.hIcon;
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass.hbrBackground = brGray0;
    wndclass.lpszMenuName = NULL;
    wndclass.lpszClassName = clpszNotesClassName;

    return RegisterClass(&wndclass);
}

HWND OpenNotesWindow(HWND parent, bool topmost) {
    auto & settings = SettingsGet();

    settings.put(cszSettingNotesOpen, true);

    DWORD dwStyle = WS_OVERLAPPEDWINDOW & ~WS_MAXIMIZEBOX;

    RECT rect = root;
    AdjustWindowRect(&rect, dwStyle, TRUE);

    // TODO clip window to monitor
    auto x = settings.get(cszSettingNotesX, CW_USEDEFAULT/* + rect.left*/);
    auto y = settings.get(cszSettingNotesY, CW_USEDEFAULT/* + rect.top*/);
    auto h = settings.get(cszSettingNotesH, 200);
    CreateWindowEx(((DWORD) topmost * WS_EX_TOPMOST) | WS_EX_COMPOSITED, clpszNotesClassName,
                   TEXT("Portal 2 Timer by Nulano - Notes"), dwStyle, x/* - rect.left*/, y/* - rect.top*/,
                   rect.right - rect.left, h, parent, NULL, hinstance, 0);
    if (!root.hwnd) {
        MessageBox(NULL, TEXT("Failed to create window"), TEXT("Portal 2 Timer by Nulano - Notes"), MB_ICONERROR | MB_OK);
        return NULL;
    }

    ShowWindow(root, SW_SHOWNORMAL);
    return root.hwnd;
}

HWND GetNotesWindow() {
    return root.hwnd;
}
