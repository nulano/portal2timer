//
// Created by Nulano on 24. 12. 2017.
//

#ifndef PORTAL2TIMER_RESOURCE_H
#define PORTAL2TIMER_RESOURCE_H

#define IDI_ICONMAIN         100

#define IDI_ICONOPEN         110
#define IDI_ICONSAVE         111
#define IDI_ICONCLOSE        112
#define IDI_ICONRELOAD       113
#define IDI_ICONHELP         114
#define IDI_ICONEDIT         115

#define IDM_MENU            2000
#define IDM_EXIT            2100
#define IDM_NOTES           2101
//#define IDM_GAME_PORTAL2    2200
#define IDM_TOPMOST         2300
#define IDM_RENAME          2301
#define IDM_ABOUT           2400
#define IDM_HOME            2401
#define IDM_SPEEDRUN_COM    2402

#endif //PORTAL2TIMER_RESOURCE_H
