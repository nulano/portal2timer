
#include "pch.h"
#include "Resource.h"
#include "IconButton.h"

template<typename Func, typename... Args>
class Lazy {
    using Type = invoke_result_t<Func, Args...>;

private:
    mutable Type instance;
    mutable bool initialized;

    Func func;
    tuple<Args...> args;

public:
    constexpr explicit Lazy(Func&& func, Args&&... args) noexcept
    : instance(), initialized(false), func(move(func)), args(forward<Args>(args)...) {};

    operator Type() const {
        if (!initialized) {
            instance = apply(func, args);
            initialized = true;
        }
        return instance;
    }
};

template<typename Func, typename... Args>
constexpr inline auto lazy(Func&& func, Args&&... args) noexcept {
    return Lazy<Func, Args...>(func, forward<Args>(args)...);
}

inline HICON LoadImageIcon(HINSTANCE hInst, LPCTSTR name, UINT type, int cx, int cy, UINT fuLoad) {
    return static_cast<HICON>(LoadImage(hInst, name, type, cx, cy, fuLoad));
}

namespace {

#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"

const auto hinstance = lazy(GetModuleHandle, (LPCTSTR) NULL);

// colors from .NET: http://www.99colors.net/dot-net-colors
const COLORREF clInvalid    = 0x1000000;
const COLORREF clBlack      =  0x000000;
const COLORREF clDark       =  0x101010;
const COLORREF clGray0      =  0x202020;
const COLORREF clGray1      =  0x404040;
const COLORREF clGray2      =  0x606060;
const COLORREF clGray3      =  0x808080;
const COLORREF clGray4      =  0xA0A0A0;
const COLORREF clWhite      =  0xFFFFFF;
const COLORREF clRed        =  0x0000FF;
const COLORREF clGreen      =  0xFF8080;
const COLORREF clBlue       =  0x40FF40;
const COLORREF clGold       =  0x00D7FF; // .NET Gold
const COLORREF clLime       =  0x2FFFAD; // .NET GreenYellow
const COLORREF clRedish     =  0x0045FF; // .NET RedOrange

const auto penNull          = lazy(CreatePen, PS_NULL, 0, 0);

const auto brBlack          = lazy(CreateSolidBrush, clBlack );
const auto brDark           = lazy(CreateSolidBrush, clDark  );
const auto brGray0          = lazy(CreateSolidBrush, clGray0 );
const auto brGray1          = lazy(CreateSolidBrush, clGray1 );
const auto brGray2          = lazy(CreateSolidBrush, clGray2 );
const auto brGray3          = lazy(CreateSolidBrush, clGray3 );
const auto brGray4          = lazy(CreateSolidBrush, clGray4 );
const auto brWhite          = lazy(CreateSolidBrush, clWhite );
const auto brRed            = lazy(CreateSolidBrush, clRed   );
const auto brGreen          = lazy(CreateSolidBrush, clGreen );
const auto brBlue           = lazy(CreateSolidBrush, clBlue  );
const auto brGold           = lazy(CreateSolidBrush, clGold  );
const auto brLime           = lazy(CreateSolidBrush, clLime  );
const auto brRedish         = lazy(CreateSolidBrush, clRedish);

const auto icoOpen         = lazy(LoadImageIcon, hinstance, MAKEINTRESOURCE(IDI_ICONOPEN),   IMAGE_ICON, 12, 12, 0);
const auto icoSave         = lazy(LoadImageIcon, hinstance, MAKEINTRESOURCE(IDI_ICONSAVE),   IMAGE_ICON, 12, 12, 0);
const auto icoClose        = lazy(LoadImageIcon, hinstance, MAKEINTRESOURCE(IDI_ICONCLOSE),  IMAGE_ICON, 12, 12, 0);
const auto icoReload       = lazy(LoadImageIcon, hinstance, MAKEINTRESOURCE(IDI_ICONRELOAD), IMAGE_ICON, 12, 12, 0);
const auto icoHelp         = lazy(LoadImageIcon, hinstance, MAKEINTRESOURCE(IDI_ICONHELP),   IMAGE_ICON, 12, 12, 0);
const auto icoEdit         = lazy(LoadImageIcon, hinstance, MAKEINTRESOURCE(IDI_ICONEDIT),   IMAGE_ICON, 12, 12, 0);

#pragma clang diagnostic pop


class Window : public RECT {
public:
    static unordered_set<Window*> instances;

    HWND hwnd = NULL;

    Window() : RECT{} {}
    explicit Window(RECT rect) : RECT(rect) {
        instances.insert(this);
    }
    ~Window() { instances.erase(this); }

    virtual HWND Create(__attribute__ ((unused)) HWND parent) { return hwnd; }

    operator HWND() { return hwnd; }

    void invalidate() { if (hwnd) RedrawWindow(hwnd, NULL, NULL, RDW_INVALIDATE); }
};
unordered_set<Window*> Window::instances;


class Label : public Window {
private:
    LPCSTR text;
    COLORREF foreground;
    HBRUSH background;
    HFONT font;
    DWORD styles;

public:
    static unordered_set<Label*> instances;

    Label() : Label({}, "", NULL) {}
    Label(RECT rect, LPCSTR text, HFONT font, COLORREF foreground = clWhite, HBRUSH background = brGray0, DWORD extraStyles = 0) :
            Window(rect), text(text), foreground(foreground), background(background), font(font), styles(extraStyles) {
        instances.insert(this);
    }
    ~Label() { instances.erase(this); }

    HWND Create(HWND parent) override {
        if (!hwnd) {
            hwnd = CreateWindowA("Static", text, WS_CHILD | WS_VISIBLE | styles,
                                 left, top, right - left, bottom - top, parent, 0, NULL, 0);
            Font(font);
        }
        return hwnd;
    }

    HBRUSH Background(HBRUSH newBg = 0) {
        HBRUSH old = background;
        if (newBg) {
            background = newBg;
            invalidate();
        }
        return old;
    }

    COLORREF Foreground(COLORREF newFg = clInvalid) {
        COLORREF old = foreground;
        if (newFg != clInvalid) {
            foreground = newFg;
            invalidate();
        }
        return old;
    }

    HFONT Font(HFONT newFont = 0) {
        HFONT old = font;
        if (newFont) {
            font = newFont;
            if (hwnd) SendMessage(hwnd, WM_SETFONT, (WPARAM) font, FALSE);
            invalidate();
        }
        return old;
    }

    void Set(LPCSTR text, COLORREF fg, HBRUSH bg) {
        SetWindowTextA(hwnd, text);
        SetColors(fg, bg);
    }

    void Set(LPCWSTR text, COLORREF fg, HBRUSH bg) {
        SetWindowTextW(hwnd, text);
        SetColors(fg, bg);
    }

    void SetColors(COLORREF newFg, HBRUSH newBg) {
        if (newFg != clInvalid) foreground = newFg;
        if (newBg) background = newBg;
        invalidate();
    }
};
unordered_set<Label*> Label::instances;


class IconButton : public Window {
private:
    LPCTSTR tooltip;
    HICON icon;
    UINT id;
    DWORD styles;

public:
    static unordered_set<IconButton*> instances;

    IconButton() : IconButton({}, _T(""), NULL, 0) {}
    IconButton(RECT rect, LPCTSTR tooltip, HICON icon, UINT id = 0, DWORD extraStyles = 0) :
            Window(rect), tooltip(tooltip), icon(icon), id(id), styles(extraStyles) {
        instances.insert(this);
    }
    ~IconButton() { instances.erase(this); }

    HWND Create(HWND parent) override {
        if (!hwnd) {
            hwnd = CreateWindow(CL_ICONBUTTON, tooltip, WS_CHILD | WS_VISIBLE | WS_TABSTOP | styles,
                                left, top, right - left, bottom - top, parent, (HMENU) (UINT_PTR) id, NULL, 0);
            Icon(icon);
        }
        return hwnd;
    }

    HICON Icon(HICON newIcon = 0) {
        HICON old = icon;
        if (newIcon) {
            icon = newIcon;
            if (hwnd) SendMessage(hwnd, BM_SETIMAGE, IMAGE_ICON, (LONG_PTR) icon);
            invalidate();
        }
        return old;
    }
};
unordered_set<IconButton*> IconButton::instances;


template <size_t count>
class PathHeader : public RECT {
private:
    Label label, path;
    IconButton buttons[count];

public:
    PathHeader() : PathHeader({}, NULL, NULL, "", {}) {}
    PathHeader(RECT rect, HFONT fntLabel, HFONT fntPath, LPCSTR title, array<tuple<LPCWSTR, HICON, UINT>, count> buttonsDecl, UINT baseId = 0) : RECT(rect) {
        label = Label({ 2, top + 2, 80,                       bottom - 2}, title,     fntLabel, clWhite, brGray0, SS_ENDELLIPSIS);
        path  = Label({80, top + 2, 398 - ((int) count) * 20, bottom - 2}, "<error>", fntPath,  clWhite, brGray0, SS_PATHELLIPSIS);
        for (size_t i = count - 1; i < count; i--) {
            int r = i == count - 1 ? right : buttons[i+1].left;
            buttons[i] = IconButton({r - 20, top, r, bottom}, get<0>(buttonsDecl[i]), get<1>(buttonsDecl[i]),
                                    baseId + get<2>(buttonsDecl[i]), (DWORD) (WS_TABSTOP | (i ? 0 : WS_GROUP)));
        }
    }

    IconButton & Button(size_t i) { return buttons[i]; }

    Label & Desc() { return label; }
    Label & Path() { return path; }
};


class SplitLabel : public RECT {
private:
    Label name, time;

public:
    SplitLabel() : SplitLabel({}, "", NULL, NULL) {}
    SplitLabel(RECT rect, LPCSTR title, HFONT font, HBRUSH background) : RECT(rect) {
        name = Label({rect.left,  rect.top, rect.right - 75, rect.bottom}, title,   font, clWhite, background, SS_NOTIFY | SS_LEFT  | SS_ENDELLIPSIS);
        time = Label({rect.right - 75, rect.top, rect.right, rect.bottom}, "---  ", font, clWhite, background, SS_NOTIFY | SS_RIGHT | SS_ENDELLIPSIS);
    }

    Label & Name() { return name; }
    Label & Time() { return time; }
};


template <size_t lines>
class SplitLabelBlock : public RECT {
private:
    SplitLabel labels[2][lines];

public:
    SplitLabelBlock(RECT rect, HFONT font, int offset) : RECT(rect) {
        for (size_t i = 0; i < lines; i++) {
            int off = (int) i * offset;
            labels[0][i] = SplitLabel({  0, top + off, 200, top + off + offset}, "", font, brGray1);
            labels[1][i] = SplitLabel({200, top + off, 400, top + off + offset}, "", font, brGray2);
            bottom = labels[0][i].bottom;
        }
    }

    SplitLabel & Chapter(size_t i) { return labels[0][i]; }
    SplitLabel & Map    (size_t i) { return labels[1][i]; }
};


class RichEdit : public Window {
private:
    COLORREF background;
    DWORD styles;

public:
    RichEdit() : RichEdit(RECT()) {}
    RichEdit(RECT rect, COLORREF background = clGray0, DWORD styles = 0) : Window(rect), background(background), styles(styles) {}

    HWND Create(HWND parent) override {
        //register // FIXME put elsewhere
        LoadLibraryW(L"Msftedit.dll");

        if (!hwnd)
            hwnd = CreateWindowExW(0/*WS_EX_TRANSPARENT*/, L"RICHEDIT50W", L"", WS_CHILD | WS_VISIBLE | styles,
                                 left, top, right - left, bottom - top, parent, 0, NULL, 0);
        Background(background);
        return hwnd;
    }

    COLORREF Background(COLORREF newBg) {
        return (COLORREF) SendMessage(hwnd, EM_SETBKGNDCOLOR, 0, newBg);
    }
};


LRESULT CALLBACK RootWndProc(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam) {
    switch (umsg) {
        case WM_CTLCOLORSTATIC: {
            HDC hdc = (HDC) wparam;
            HWND ctl = (HWND) lparam;

            SetBkMode(hdc, TRANSPARENT);

            for (auto lbl : Label::instances) {
                if (lbl->hwnd == ctl) {
                    SetTextColor(hdc, lbl->Foreground());
                    return (LRESULT) lbl->Background();
                }
            }

            // return white to help find errors
            return (LRESULT) (HBRUSH) brWhite;
        }
        case IBM_COLOR:
            switch (wparam) {
                default:
                case IBMC_BG:       return (INT_PTR) (HBRUSH) brGray0;
                case IBMC_FG_IDLE:  return (INT_PTR) (HBRUSH) brGray1;
                case IBMC_FG_FOCUS: return (INT_PTR) (HBRUSH) brGray2;
                case IBMC_FG_HOVER: return (INT_PTR) (HBRUSH) brGray3;
            }
        default:
            return DefWindowProc(hwnd, umsg, wparam, lparam);
    }
}

}
