
#include "pch.h"
#include "game.h"
#include "Demo.h"

const DemoAdjuster p2_adjust_sp_a1_intro1 = [](const Demo & demo, long long tickStart, long long tickEnd){
    // verified with SAR: https://github.com/NeKzor/SourceAutoRecord/blob/master/src/Features/Speedrun/Rules/Portal2Rules.cpp
    // (2 ticks earlier than P2LT)
    for (auto const & message : demo.messages) {
        if (message.type != dem_packet || message.tick < 0) continue;
        DemoPacket packet = get<dem_packet>(message.data);
        float x = packet.info.player1.viewOrigin.x - (-8709.20f);
        float y = packet.info.player1.viewOrigin.y - (+1690.07f);
        float z = packet.info.player1.viewOrigin.z -   (+28.00f);
        if (x*x < 0.02f*0.02f && y*y < 0.02f*0.02f && z*z < 0.5f*0.5f) {
            return tickEnd - message.tick + 1;
        }
    }
    return tickEnd - tickStart;
};
const DemoAdjuster p2_adjust_sp_a4_finale4 = [](const Demo & demo, long long tickStart, long long tickEnd){
    // verified with SAR: https://github.com/NeKzor/SourceAutoRecord/blob/master/src/Features/Speedrun/Rules/Portal2Rules.cpp
    // (8 ticks earlier than P2LT)
    for (auto const & message : demo.messages) {
        if (message.type != dem_consolecmd || message.tick < 0) continue;
        if (get<dem_consolecmd>(message.data).command ==
                string("hud_subtitles resource/cheaptitles/sp_a4_finale4_b1_captions.txt")) {
            return message.tick - 2 - tickStart;
        }
    }
    return tickEnd - tickStart;
};

Map p2_chapter1[] = {
        {"sp_a1_intro1", "Container Ride", p2_adjust_sp_a1_intro1},
        {"sp_a1_intro2", "Portal Carousel"},
        {"sp_a1_intro3", "Portal Gun"},
        {"sp_a1_intro4", "Smooth Jazz"},
        {"sp_a1_intro5", "Cube Momentum"},
        {"sp_a1_intro6", "Future Starter"},
        {"sp_a1_intro7", "Secret Panel"},
        {"sp_a1_wakeup", "Wakeup"},
        {"sp_a2_intro", "Incinerator"}
};
Map p2_chapter2[] = {
        {"sp_a2_laser_intro", "Laser Intro"},
        {"sp_a2_laser_stairs", "Laser Stairs"},
        {"sp_a2_dual_lasers", "Dual Lasers"},
        {"sp_a2_laser_over_goo", "Laser Over Goo"},
        {"sp_a2_catapult_intro", "Catapult Intro"},
        {"sp_a2_trust_fling", "Trust Fling"},
        {"sp_a2_pit_flings", "Pit Flings"},
        {"sp_a2_fizzler_intro", "Fizzler Intro"}
};
Map p2_chapter3[] = {
        {"sp_a2_sphere_peek", "Ceiling Catapult"},
        {"sp_a2_ricochet", "Ricochet"},
        {"sp_a2_bridge_intro", "Bridge Intro"},
        {"sp_a2_bridge_the_gap", "Bridge the Gap"},
        {"sp_a2_turret_intro", "Turret Intro"},
        {"sp_a2_laser_relays", "Laser Relays"},
        {"sp_a2_turret_blocker", "Turret Blocker"},
        {"sp_a2_laser_vs_turret", "Laser vs. Turret"},
        {"sp_a2_pull_the_rug", "Pull the Rug"}
};
Map p2_chapter4[] = {
        {"sp_a2_column_blocker", "Column Blocker"},
        {"sp_a2_laser_chaining", "Laser Chaining"},
        {"sp_a2_triple_laser", "Triple Laser"},
        {"sp_a2_bts1", "Jail Break"},
        {"sp_a2_bts2", "Escape"}
};
Map p2_chapter5[] = {
        {"sp_a2_bts3", "Turret Factory"},
        {"sp_a2_bts4", "Turret Sabotage"},
        {"sp_a2_bts5", "Neurotoxin Sabotage"},
        {"sp_a2_bts6", "Tube Ride"},
        {"sp_a2_core", "Core"}
};
Map p2_chapter6[] = {
        {"sp_a3_00", "Long Fall"},
        {"sp_a3_01", "Underground"},
        {"sp_a3_03", "Cave Johnson"},
        {"sp_a3_jump_intro", "Repulsion Intro"},
        {"sp_a3_bomb_flings", "Bomb Flings"},
        {"sp_a3_crazy_box", "Crazy Box"},
        {"sp_a3_transition01", "PotatOS"}
};
Map p2_chapter7[] = {
        {"sp_a3_speed_ramp", "Propulsion Intro"},
        {"sp_a3_speed_flings", "Propulsion Flings"},
        {"sp_a3_portal_intro", "Conversion Intro"},
        {"sp_a3_end", "Three Gels"}
};
Map p2_chapter8[] = {
        {"sp_a4_intro", "Test"},
        {"sp_a4_tb_intro", "Funnel Intro"},
        {"sp_a4_tb_trust_drop", "Ceiling Button"},
        {"sp_a4_tb_wall_button", "Wall Button"},
        {"sp_a4_tb_polarity", "Polarity"},
        {"sp_a4_tb_catch", "Funnel Catch"},
        {"sp_a4_stop_the_box", "Stop the Box"},
        {"sp_a4_laser_catapult", "Laser Catapult"},
        {"sp_a4_laser_platform", "Laser Platform"},
        {"sp_a4_speed_tb_catch", "Propulsion Catch"},
        {"sp_a4_jump_polarity", "Repulsion Polarity"}
};
Map p2_chapter9[] = {
        {"sp_a4_finale1", "Finale 1"},
        {"sp_a4_finale2", "Finale 2"},
        {"sp_a4_finale3", "Finale 3"},
        {"sp_a4_finale4", "Finale 4", p2_adjust_sp_a4_finale4}
};

Chapter p2_chapters[] = {
        {p2_chapter1, "1. The Courtesy Call"},
        {p2_chapter2, "2. The Cold Boot"},
        {p2_chapter3, "3. The Return"},
        {p2_chapter4, "4. The Surprise"},
        {p2_chapter5, "5. The Escape"},
        {p2_chapter6, "6. The Fall"},
        {p2_chapter7, "7. The Reunion"},
        {p2_chapter8, "8. The Itch"},
        {p2_chapter9, "9. The Part Where..."},
};

Game p2_game = {p2_chapters, "portal2", "Portal 2"};

Game& GetGame_Portal2() {
    return p2_game;
}
