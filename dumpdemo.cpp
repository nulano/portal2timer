#include <iostream>
#include "Demo.h"

using namespace std;

int main() {
    int argc; LPWSTR * argv = CommandLineToArgvW(GetCommandLineW(), &argc);
    if (argc < 2) {
        cout << "No demo specified." << endl;
        return 1;
    }

    wcout << L"Opening file: " << argv[1] << endl;
    HANDLE file = CreateFileW(argv[1], GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);
    if (file == INVALID_HANDLE_VALUE) {
        cout << "Failed to open file: " << GetLastError() << endl;
        return 1;
    }

    cout << "Parsing demo." << endl;
    Demo demo = {};
    if (!LoadDemo(demo, file)) {
        cout << "Failed to parse demo." << endl;
        CloseHandle(file);
        return 1;
    }

    cout << endl;
    cout << "Game: " << demo.header.GameDirectory << endl;
    cout << "Map: " << demo.header.MapName << endl;
    cout << "Frames: " << demo.header.Frames << endl;
    cout << "Ticks: " << demo.header.Ticks << endl;
    cout << "PlaybackTime: " << demo.header.PlaybackTime << endl;
    cout << endl;
    for (const DemoMessage & message : demo.messages) {
        cout << "[" << message.tick << "] ";
        visit([](auto&& msg) {
            using T = decay_t<decltype(msg)>;
            if constexpr (is_same_v<T, std::monostate>) {
                cout << "<none>" << endl;
            } else if constexpr (is_same_v<T, DemoSignOn>) {
                const DemoSignOn& signon = msg;
                cout << "signon: size=" << signon.packet.data.size() << endl;
                // TODO net data
            } else if constexpr (is_same_v<T, DemoPacket>) {
                const DemoPacket& packet = msg;
                cout << "packet: size=" << packet.data.size() << endl;
                cout << "  player1: ";
                cout << "origin1: ";
                cout << "X=" << packet.info.player1.viewOrigin.x << ", ";
                cout << "Y=" << packet.info.player1.viewOrigin.y << ", ";
                cout << "Z=" << packet.info.player1.viewOrigin.z << "; ";
                cout << "angle1: ";
                cout << "X=" << packet.info.player1.viewAngles.x << ", ";
                cout << "Y=" << packet.info.player1.viewAngles.y << ", ";
                cout << "Z=" << packet.info.player1.viewAngles.z << endl;
                cout << "  player2: ";
                cout << "origin1: ";
                cout << "X=" << packet.info.player2.viewOrigin.x << ", ";
                cout << "Y=" << packet.info.player2.viewOrigin.y << ", ";
                cout << "Z=" << packet.info.player2.viewOrigin.z << "; ";
                cout << "angle1: ";
                cout << "X=" << packet.info.player2.viewAngles.x << ", ";
                cout << "Y=" << packet.info.player2.viewAngles.y << ", ";
                cout << "Z=" << packet.info.player2.viewAngles.z << endl;
                // TODO net data
            } else if constexpr (is_same_v<T, DemoSyncTick>) {
                cout << "synctick" << endl;
            } else if constexpr (is_same_v<T, DemoConsoleCmd>) {
                const DemoConsoleCmd& cmd = msg;
                cout << "consolecmd: " << cmd.command << endl;
            } else if constexpr (is_same_v<T, DemoUserCmd>) {
                const DemoUserCmd& cmd = msg;
                cout << "usercmd: sequence=" << cmd.sequence;
                if (cmd.sequenceOpt.has_value()) cout << ", seq=" << cmd.sequenceOpt.value();
                if (cmd.tickCount.has_value()) cout << ", tick=" << cmd.tickCount.value();
                if (cmd.viewAngleX.has_value()) cout << ", viewX=" << cmd.viewAngleX.value();
                if (cmd.viewAngleY.has_value()) cout << ", viewY=" << cmd.viewAngleY.value();
                if (cmd.viewAngleZ.has_value()) cout << ", viewZ=" << cmd.viewAngleZ.value();
                if (cmd.moveForward.has_value()) cout << ", moveFwd=" << cmd.moveForward.value();
                if (cmd.moveSide.has_value()) cout << ", moveSide=" << cmd.moveSide.value();
                if (cmd.moveUp.has_value()) cout << ", moveUp=" << cmd.moveUp.value();
                if (cmd.buttons.has_value()) cout << ", btn=" << cmd.buttons.value();
                if (cmd.impulse.has_value()) cout << ", impulse=" << cmd.impulse.value();
                // TODO weaponSelect
                if (cmd.mouseDx.has_value()) cout << ", mouseDx=" << cmd.mouseDx.value();
                if (cmd.mouseDy.has_value()) cout << ", mouseDy=" << cmd.mouseDy.value();
                // TODO entityGroundContact
                cout << endl;
            } else if constexpr (is_same_v<T, DemoDataTables>) {
                const DemoDataTables& tables = msg;
                cout << "datatables: size=" << tables.data.size() << endl;
            } else if constexpr (is_same_v<T, DemoStop>) {
                cout << "stop" << endl;
            } else if constexpr (is_same_v<T, DemoCustomData>) {
                const DemoCustomData& data = msg;
                cout << "customdata: unk=" << data.unknown << ", size=" << data.data.size() << endl;
            } else if constexpr (is_same_v<T, DemoStringTables>) {
                const DemoStringTables& tables = msg;
                cout << "stringtables:" << endl;
                for (const auto& table : tables.tables) {
                    cout << "  " << table.first << ":" << endl;
                    for (const auto& entry : table.second.entries)
                        cout << "    " << entry.first << ": size=" << entry.second.size() << endl;

                    for (const auto& entry : table.second.clientEntries)
                        cout << "    (client) " << entry.first << ": size=" << entry.second.size() << endl;
                }
            } else cout << "<error>" << endl;
        }, message.data);
    }

    CloseHandle(file);
    return 0;
}
