//
// Created by Nulano on 20. 12. 2017.
//

#include "pch.h"
#include "Demo.h"
#include "bitstream.h"

#define IfReadFail(x) ReadFile(fileHandle, &(x), sizeof(x), &read, NULL); if (read != sizeof(x))
#define IfReadArrayFail(x) ReadFile(fileHandle, &(x)[0], sizeof(x), &read, NULL); if (read != sizeof(x))
#define IfReadToArrayFail(x, n) ReadFile(fileHandle, (x), (n), &read, NULL); if (read != (n))

bool LoadDemo(Demo &demo, const HANDLE &fileHandle) {
    DWORD read = 0;

    IfReadArrayFail(demo.Magic) return false;
    if (strcmp("HL2DEMO", demo.Magic) != 0) return false; // magic number mismatch

    IfReadFail(demo.header.DemoProtocol) return false;
    IfReadFail(demo.header.NetworkProtocol) return false;
    IfReadArrayFail(demo.header.ServerName) return false;
    IfReadArrayFail(demo.header.ClientName) return false;
    IfReadArrayFail(demo.header.MapName) return false;
    IfReadArrayFail(demo.header.GameDirectory) return false;
    IfReadFail(demo.header.PlaybackTime) return false;
    IfReadFail(demo.header.Ticks) return false;
    IfReadFail(demo.header.Frames) return false;
    IfReadFail(demo.header.SignOnLength) return false;

    bool done = false;
    while (!done) {
        DemoMessage message = DemoMessage();
        IfReadFail(message.type) return false;
        IfReadFail(message.tick) return false;
        IfReadFail(message.playerSlot) return false;

        // if (netprotocol < 36 (?) && type == dem_customdata)
        //    type = dem_stringtables;

        switch (message.type) {
            case dem_synctick: {
                message.data = DemoSyncTick{};
                break;
            }
            case dem_stop: {
                done = true;
                message.data = DemoStop{};
                break;
            }
            case dem_consolecmd: {
                uint32_t length; IfReadFail(length) return false;
                vector<char> data(length);
                IfReadToArrayFail(data.data(), length) return false;
                string cmd(data.data(), length - 1);
                message.data = DemoConsoleCmd{cmd};
                break;
            }
            case dem_usercmd: {
                DemoUserCmd cmd = {};
                IfReadFail(cmd.sequence) return false;
                
                uint32_t length; IfReadFail(length) return false;
                vector<uint8_t> data(length);
                IfReadToArrayFail(data.data(), length) return false;
                bitstream bits(data);
                
                if (bits.get<bool>())
                    bits >> cmd.sequenceOpt;
                if (bits.get<bool>())
                    bits >> cmd.tickCount;
                if (bits.get<bool>())
                    bits >> cmd.viewAngleX;
                if (bits.get<bool>())
                    bits >> cmd.viewAngleY;
                if (bits.get<bool>())
                    bits >> cmd.viewAngleZ;
                if (bits.get<bool>())
                    bits >> cmd.moveForward;
                if (bits.get<bool>())
                    bits >> cmd.moveSide;
                if (bits.get<bool>())
                    bits >> cmd.moveUp;
                if (bits.get<bool>())
                    bits >> cmd.buttons;
                if (bits.get<bool>())
                    bits >> cmd.impulse;
                // TODO weaponSelect
                if (bits.get<bool>())
                    bits >> cmd.mouseDx;
                if (bits.get<bool>())
                    bits >> cmd.mouseDy;
                // TODO entitygroundContact

                message.data = cmd;
                break;
            }
            case dem_customdata: {
                // TODO implement properly
                DemoCustomData data = {};
                IfReadFail(data.unknown) return false;
                uint32_t size; IfReadFail(size) return false;
                data.data.resize(size);
                IfReadToArrayFail(data.data.data(), size) return false;
                message.data = data;
                break;
            }
            case dem_stringtables: {
                DemoStringTables tables = {};
                uint32_t size; IfReadFail(size) return false;
                vector<uint8_t> data(size);
                IfReadToArrayFail(data.data(), size) return false;
                bitstream bits(data);
                for (size_t i = 0, numTables = bits.get<uint8_t>(); i < numTables; i++) {
                    string tableName;
                    bits >> tableName;
                    DemoStringTables::Table table = {};

                    size_t numEntries = bits.get<uint16_t>();
                    for (size_t j = 0; j < numEntries; j++) {
                        string entryName;
                        vector<uint8_t> entryData;
                        bits >> entryName;
                        if (bits.get<bool>())
                            entryData.resize(bits.get<uint16_t>());
                        for (uint8_t& byte : entryData)
                            bits >> byte;
                        table.entries[entryName] = move(entryData);
                    }

                    size_t numClientEntries = 0;
                    if (bits.get<bool>())
                        numClientEntries = bits.get<uint16_t>();
                    for (size_t j = 0; j < numClientEntries; j++) {
                        string entryName;
                        vector<uint8_t> entryData;
                        bits >> entryName;
                        if (bits.get<bool>())
                            entryData.resize(bits.get<uint16_t>());
                        for (uint8_t& byte : entryData)
                            bits >> byte;
                        table.clientEntries[entryName] = move(entryData);
                    }

                    tables.tables[tableName] = move(table);
                }

                message.data = tables;
                break;
            }
            case dem_datatables: {
                // TODO implement properly
                DemoDataTables tables = {};
                uint32_t size; IfReadFail(size) return false;
                tables.data.resize(size);
                IfReadToArrayFail(tables.data.data(), size) return false;
                message.data = tables;
                break;
            }
            case dem_signon:
            case dem_packet: {
                // TODO implement properly
                DemoPacket packet = {};
                IfReadFail(packet.info) return false;
                IfReadFail(packet.sequenceIn) return false;
                IfReadFail(packet.sequenceOut) return false;
                uint32_t size; IfReadFail(size) return false;
                packet.data.resize(size);
                IfReadToArrayFail(packet.data.data(), size) return false;
                bitstream bits(packet.data);

                if (message.type == dem_signon)
                    message.data = DemoSignOn{packet};
                else
                    message.data = packet;
                break;
            }
            default:
                return false;
        }

        demo.messages.push_back(move(message));
    }

    return true;
}
