
#include "pch.h"

#include "game.h"
#include "timer.h"
#include "notes.h"

#include <sys/time.h>
#include "Version.h"
#include "Resource.h"

#include "ui.h"

#define IDC_GAME_SELECT     0x4000
#define IDC_PB_OPEN         0x4010
#define IDC_PB_CLOSE        0x4011
#define IDC_BEST_OPEN       0x4020
#define IDC_BEST_CLOSE      0x4021
#define IDC_BEST_SAVE       0x4022
#define IDC_CURRENT_CLOSE   0x4031
#define IDC_CURRENT_SAVE    0x4032
#define IDC_CURRENT_RENAME  0x4033

const LPCTSTR clpszRootClassName = TEXT("NulanoPortal2Timer");

//FIXME duplicated variables
const int szConfig = 16, szTimeLbl = 16, szTimeBg = 48, szTimeMd = 40, szTimeSm = 32, szMap = 16, szStatus = 24;
const size_t chapterLines = 12;

//                                         size            weight I  U  S
const HFONT fntConfig       = CreateFont(szConfig,  0, 0, 0, 400, 0, 0, 0, 1, 0, 0, 0, 0, L"Segoe UI");
const HFONT fntConfigPath   = CreateFont(szConfig,  0, 0, 0, 400, 1, 0, 0, 1, 0, 0, 0, 0, L"Segoe UI");
const HFONT fntTimeLbl      = CreateFont(szTimeLbl, 0, 0, 0, 700, 0, 0, 0, 1, 0, 0, 0, 0, L"Segoe UI");
const HFONT fntTimeBg       = CreateFont(szTimeBg,  0, 0, 0, 700, 0, 0, 0, 1, 0, 0, 0, 0, L"Segoe UI");
const HFONT fntTimeMd       = CreateFont(szTimeMd,  0, 0, 0, 700, 0, 0, 0, 1, 0, 0, 0, 0, L"Segoe UI");
const HFONT fntTimeSm       = CreateFont(szTimeSm,  0, 0, 0, 700, 0, 0, 0, 1, 0, 0, 0, 0, L"Segoe UI");
const HFONT fntMaps         = CreateFont(szMap,     0, 0, 0, 700, 0, 0, 0, 1, 0, 0, 0, 0, L"Segoe UI");
const HFONT fntStatus       = CreateFont(szStatus,  0, 0, 0, 700, 0, 0, 0, 1, 0, 0, 0, 0, L"Segoe UI");

static auto root = Window({0, 0, 400, 0});

static auto phGameDir    = PathHeader<1>({0, root.top,            root.right, root.top            + 20},
                                fntConfig, fntConfigPath, "Game Dir:",
                               {{{L"Change Game Directory", icoOpen, IDC_GAME_SELECT}}});
static auto phSplitsPb   = PathHeader<2>({0, phGameDir.bottom,    root.right, phGameDir.bottom    + 20},
                                fntConfig, fntConfigPath, "PB:",
                               {{{L"Open Run...", icoOpen, IDC_PB_OPEN},
                                 {L"Unload Run",  icoClose, IDC_PB_CLOSE}}});
static auto phSplitsBest = PathHeader<3>({0, phSplitsPb.bottom,   root.right, phSplitsPb.bottom   + 20},
                                fntConfig, fntConfigPath, "Best Splits:",
                               {{{L"Save Updated Best Splits...", icoSave, IDC_BEST_SAVE},
                                 {L"Open Best Splits...", icoOpen, IDC_BEST_OPEN},
                                 {L"Unload Best Splits",  icoClose, IDC_BEST_CLOSE}}});
static auto phCurrent    = PathHeader<2>({0, phSplitsBest.bottom, root.right, phSplitsBest.bottom + 20},
                                fntConfig, fntConfigPath, "Current Run:",
                                  // TODO add {L"Rename Current Run...", icoPencil, IDC_CURRENT_RENAME}
                               {{{L"Save Current Run...", icoSave, IDC_CURRENT_SAVE},
                                 {L"Reset",               icoClose, IDC_CURRENT_CLOSE}}});

static auto lblTimeEstLbl  = Label({  0, phCurrent.bottom + 50,    200, phCurrent.bottom + 50 + szTimeLbl},    "Estimated Time",       fntTimeLbl, clWhite, brGray0, SS_CENTER);
static auto lblTimeRtaLbl  = Label({200, phCurrent.bottom + 50,    400, phCurrent.bottom + 50 + szTimeLbl},    "Time with Loads",      fntTimeLbl, clWhite, brGray0, SS_CENTER);
static auto lblTimeEst     = Label({  0, lblTimeEstLbl.bottom,     175, lblTimeEstLbl.bottom + szTimeBg},      "0:00:00",              fntTimeBg,  clWhite, brGray0, SS_RIGHT | SS_ENDELLIPSIS);
static auto lblTimeRta     = Label({175, lblTimeRtaLbl.bottom,     375, lblTimeRtaLbl.bottom + szTimeBg},      "0:00:00",              fntTimeBg,  clWhite, brGray0, SS_RIGHT | SS_ENDELLIPSIS);
static auto lblTimeLastLbl = Label({  0, lblTimeEst.bottom + 30,   200, lblTimeEst.bottom + 30 + szTimeLbl},   "Time After Last Demo", fntTimeLbl, clWhite, brGray0, SS_CENTER);
static auto lblTimeBestLbl = Label({200, lblTimeRta.bottom + 30,   400, lblTimeRta.bottom + 30 + szTimeLbl},   "Best Possible Time",   fntTimeLbl, clWhite, brGray0, SS_CENTER);
static auto lblTimeLast    = Label({  0, lblTimeLastLbl.bottom,    175, lblTimeLastLbl.bottom + szTimeMd},     "0:00.000",             fntTimeMd,  clWhite, brGray0, SS_RIGHT | SS_ENDELLIPSIS);
static auto lblTimeBest    = Label({200, lblTimeBestLbl.bottom,    375, lblTimeBestLbl.bottom + szTimeMd},     "0:00.000",             fntTimeMd,  clWhite, brGray0, SS_RIGHT | SS_ENDELLIPSIS);
static auto lblTimeLastSpl = Label({  0, lblTimeLast.bottom,       175, lblTimeLast.bottom + szTimeSm},        "+0.000",               fntTimeSm,  clWhite, brGray0, SS_RIGHT | SS_ENDELLIPSIS);
static auto lblTimeBestSpl = Label({200, lblTimeBest.bottom,       375, lblTimeBest.bottom + szTimeSm},        "+0.000",               fntTimeSm,  clWhite, brGray0, SS_RIGHT | SS_ENDELLIPSIS);

static auto lblBlock       = SplitLabelBlock<chapterLines>({0, lblTimeLastSpl.bottom + 50, 400, 0}, fntMaps, szMap);

static auto lblNextDemo    = Label({0, lblBlock.bottom,    398, lblBlock.bottom    + 18}, "<error>", fntMaps,   clWhite, brGray0, SS_RIGHT);
static auto lblStatus      = Label({0, lblNextDemo.bottom, 400, lblNextDemo.bottom + 24}, "<error>", fntStatus, clWhite, brRed,   SS_CENTER);

static auto __attribute((__unused__)) __bottom = root.bottom = lblStatus.bottom;

static wiptree settings;

// TODO clean variable names
static set<wstring> demosIgnored;

int status = 0;
bool savedRun = false, savedBest = false;
wstring attempt = L""; // current run
wstring demoWaiting = L"";
int demosWaitingCount = 0;
int demosProcessed = 0;
MapId mapSelected;
Game * game = GetGame("portal2");

long long msRtaStart = 0, msRtaEnd = 0, msLastDemo = 0;
long long ticksAdjStart = 0, ticksAdjEnd = 0;


//https://stackoverflow.com/a/4609795/1648883
template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

//https://stackoverflow.com/a/19555210/1648883
long long GetTimeMillis() {
    struct timeval tp = {};
    gettimeofday(&tp, NULL);
    return (long long) tp.tv_sec * 1000L + tp.tv_usec / 1000;
}


typedef struct TicksOpt_ {
    long long ticks;
    bool valid;
    TicksOpt_() : ticks(0), valid(true) {}
    TicksOpt_(long long t) : ticks(t), valid(t != 0) {}
    TicksOpt_(long long t, bool v) : ticks(t), valid(v) {}
    TicksOpt_& add(long long t) {
        return add(t, t != 0);
    }
    TicksOpt_& add(long long t, bool v) {
        ticks += t;
        valid &= v;
        return *this;
    }
    TicksOpt_& addSplit(long long t1, long long t0) {
        if (t1 != 0) add(t1 - t0, t0 != 0);
        return *this;
    }
    operator long long () const { return ticks * valid; }
    operator bool () const { return valid; }
    long long time() const { return ticks * valid * 1000 / tps; }
} TicksOpt;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
string FormatTime(long long time, int digitsMillis = 0, bool forceSign = false, int minSectors = 1) {
    long long tm = abs(time);
    int milli = 0;
    switch (digitsMillis) {
        default:
        case 0: milli = 0; break;
        case 1: milli = (tm / 100) % 10; break;
        case 2: milli = (tm / 10) % 100; break;
        case 3: milli = (tm / 1) % 1000; break;
    }
    int sec = (tm / 1000) % 60;
    int min = (tm / 1000 / 60) % 60;
    int hour = (tm / 1000 / 60 / 60);

    int sectors = 1;
    if (min) sectors = 2;
    if (hour) sectors = 3;
    sectors = max(sectors, minSectors);

    char buf[100];
    switch (sectors) {
        case 1: sprintf(buf, "%d", sec); break;
        case 2: sprintf(buf, "%d:%.2d", min, sec); break;
        default:
        case 3: sprintf(buf, "%d:%.2d:%.2d", hour, min, sec); break;
    }
    char buf2[100];
    switch (digitsMillis) {
        default:
        case 0: strcpy(buf2, ""); break;
        case 1: sprintf(buf2, ".%.1d", milli); break;
        case 2: sprintf(buf2, ".%.2d", milli); break;
        case 3: sprintf(buf2, ".%.3d", milli); break;
    }
    return string(time < 0 ? "-":(forceSign ? "+":""))+buf+buf2;
}
#pragma clang diagnostic pop


wstring SettingsFilename() {
    wchar_t dir[1024];
    if (!GetModuleFileNameW(NULL, dir, 1024))
        return L""; // TODO
    wstring file(dir);
    return file.substr(0, file.rfind(L'.')).append(L"_settings.xml");
}

wiptree & SettingsGet() {
    return settings;
}

bool SettingsLoad() {
    wstring wfilename = SettingsFilename();
    string filename = wstring_convert<codecvt_utf8<wchar_t>>().to_bytes(wfilename.c_str());

    try { read_xml(filename, settings, xml_parser::trim_whitespace); }
    catch (...) { return false; }

    return true;
}

bool SettingsSave() {
    wstring wfilename = SettingsFilename();
    string filename = wstring_convert<codecvt_utf8<wchar_t>>().to_bytes(wfilename.c_str());

    try { write_xml(filename, settings, locale(), xml_writer_settings<wstring>(' ', 4)); }
    catch (...) { return false; }

    return true;
}

bool DoRename() {
    settings.put(cszSettingDoRename, settings.get<bool>(cszSettingDoRename, true));
    return settings.get<bool>(cszSettingDoRename, false);
}

bool Topmost() {
    settings.put(cszSettingTopmost, settings.get<bool>(cszSettingTopmost, true));
    return settings.get<bool>(cszSettingTopmost, false);
}


void UpdateTimeLabels();


template <uint32_t Map::*value>
void ClearSplits() {
    MapId i = {0, 0};
    do { game->getMap(i)->*value = 0; }
    while (game->advance(i));
}

template <uint32_t Map::*value>
bool LoadSplits(HANDLE csvFile) {
    ClearSplits<value>();

    MapId hint = {0, 0};
    DWORD off = 0;
    bool fail = false;
    int maps = 0;
    while (true) {
        char buf[256];
        DWORD read = 0;
        SetFilePointer(csvFile, off, NULL, FILE_BEGIN);
        bool readSuccess = ReadFile(csvFile, &buf, 128, &read, NULL) != 0;
        if (read == 0) break;
        if (!readSuccess) {
            fail = true;
            break;
        }
        buf[read] = '\0';
        char * offCr = static_cast<char*>(memchr(buf, '\r', read));
        char * offLf = static_cast<char*>(memchr(buf, '\n', read));
        switch (((offCr != NULL) << 1) | ((offLf != NULL) << 0)) {
            default:
            case 0: break;
            case 1: off += (offLf - buf) + 1; break;
            case 2: off += (offCr - buf) + 1; break;
            case 3: off += (min(offCr, offLf) - buf) + 2; break;
        }
        char map[70]; map[0] = '\0';
        long long tick1 = 0, tick2 = 0, ticks = 0;
        int cols = sscanf(buf, " %64[^,],%lld,%lld", map, &tick1, &tick2);
        if (cols == 2) {
            ticks = tick1;
        } else if (cols == 3) {
            ticks = tick2 - tick1;
        } else if (cols == 1) {
            continue; // support for 1st line csv header
        } else {
            fail = true;
            break;
        }
        if (ticks <= 0) {
            fail = true;
            break;
        }

        Map* m = game->getMap(hint = game->findMap(map, hint));
        if (m != nullptr) {
            m->*value += ticks;
            maps++;
        } else {
            fail = true;
            break;
        }
    }

    if (maps == 0)
        fail = true;

    if (fail) ClearSplits<value>();
    return !fail;
}

bool SaveSplits(wstring filepath, bool best) {
    if (best) {
        settings.put(cszSettingGameBest, filepath);
        SetWindowTextW(phSplitsBest.Path(), filepath.c_str());
    } /*else {
        settings.put(cszSettingGamePb, filepath);
        SetWindowTextW(phSplitsPb.Path(), filepath.c_str());
    }*/

    bool valid = false; // FIXME condition used in WM_COMMAND is (status != 0)
    MapId i = {0, 0};
    do {
        if (game->getMap(i)->ticksCurrent) {
            valid = true;
            break;
        }
    } while (game->advance(i));
    if (!valid) return false;

    // TODO keep old backup if rewriting
    HANDLE file = CreateFileW(filepath.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);
    if (file == INVALID_HANDLE_VALUE) return false;
    struct close { const HANDLE f; close(HANDLE f) : f(f) {} ~close() { CloseHandle(f); } } close_(file);

    i = {0, 0};
    do {
        uint32_t ticks = 0;
        Map& m = *game->getMap(i);
        if (best && m.ticksBest && m.ticksCurrent)
            ticks = min(m.ticksBest, m.ticksCurrent);
        else if (best && m.ticksBest) ticks = m.ticksBest;
        else if (m.ticksCurrent) ticks = m.ticksCurrent;
        else continue;
        if (ticks) {
            ostringstream line;
            line << m.file << ", " << ticks << endl;
            string str = line.str();
            DWORD len;
            if (!WriteFile(file, str.c_str(), (DWORD) str.size(), &len, NULL) || len != str.size())
                return false;
        }
    } while (game->advance(i));

    return true;
}


void FindIgnoredDemos();

void ResetTime() {
    status = 0;
    attempt = L"";
    SetWindowTextW(phCurrent.Path(), L"<auto>");
    demoWaiting = L"";
    demosWaitingCount = 0;
    demosProcessed = 0;

//    for (int i = 0; i < chapterLines; i++)
//        UpdateMapLabels(i);
    mapSelected = {0, 0};

    ClearSplits<&Map::ticksCurrent>();

    msRtaStart = 0;
    msRtaEnd = 0;
    ticksAdjStart = 0;
    ticksAdjEnd = 0;

    demosIgnored.clear();
    FindIgnoredDemos();
}

/// Doesn't call ResetTime()
void StartTime() {
    savedRun = false;
    savedBest = false;

    status = 1;

    if (!attempt.size()) {
        time_t now;
        time(&now);
        wchar_t buf[sizeof "20111008-0707"];
        wcsftime(buf, sizeof buf, L"%Y%m%d-%H%M", gmtime(&now));
        attempt = wstring(buf);
        SetWindowTextW(phCurrent.Path(), buf);
    }

//    ResetTime();

    msLastDemo = GetTimeMillis();
    msRtaStart = GetTimeMillis();
}

void StopTimer() {
    status = 2;
    msRtaEnd = GetTimeMillis();
}


void FindIgnoredDemos() {
    wstring findFilename = settings.get<wstring>(cszSettingGameDir) + L"*.dem";
    WIN32_FIND_DATA finddata = {};
    HANDLE findHandle = FindFirstFileW(findFilename.c_str(), &finddata);
    if (findHandle != INVALID_HANDLE_VALUE)
        do {
            demosIgnored.insert(wstring(finddata.cFileName));
        } while (FindNextFileW(findHandle, &finddata));
}

wstring NextDemoName() {
    int demoNum = demosProcessed + 1;
    wostringstream filename; filename << attempt;
    if (demoNum >= 2)
        filename << "_" << demoNum;
    filename << ".dem";
    return filename.str();
}

void ProcessDemo(const wstring & filename, const wstring & filepath, const HANDLE & file) {
    Demo demo = {};
    if (!LoadDemo(demo, file))
        return;

    MapId mapId = game->findMap(demo.header.MapName);
    Map* map = game->getMap(mapId);
    if (map == nullptr) {
        demosIgnored.insert(filename);
        return;
    }

    long long tickStart = 100000000000000, tickEnd = -1;
    for (auto const & message : demo.messages) {
        if (message.type == dem_packet && message.tick >= 0) {
            tickStart = min(tickStart, (long long) message.tick);
            tickEnd   = max(tickEnd,   (long long) message.tick);
        }
    }
    long long ticks = tickEnd - tickStart;
    if (map->adjust) {
        long long ticksAdjusted = map->adjust(demo, tickStart, tickEnd);
        if (mapId == MapId{0, 0} && map->ticksCurrent == 0)
            ticksAdjStart = ticksAdjusted - ticks;
        if (map == &game->lastMap())
            ticksAdjEnd = ticksAdjusted - ticks;
        ticks = ticksAdjusted;
    }
    map->ticksCurrent += ticks;

    mapSelected = mapId;

    msLastDemo = GetTimeMillis();
    if (map == &game->lastMap())
        StopTimer();

    wstring filename2 = NextDemoName();
    wstring filepath2 = settings.get<wstring>(cszSettingGameDir) + filename2;
    CloseHandle(file); // must close before moving file
    if (DoRename() && MoveFileExW(filepath.c_str(), filepath2.c_str(), MOVEFILE_WRITE_THROUGH))
        demosIgnored.insert(filename2);
    else demosIgnored.insert(filename);
    demosProcessed++;

    ScrollNotes(game->getMap(mapSelected));
}

void ProcessDemos() {
    demosWaitingCount = 0;
    wstring findFilename = settings.get<wstring>(cszSettingGameDir);
    findFilename += L"*.dem";
    WIN32_FIND_DATA finddata = {};
    HANDLE findHandle = FindFirstFileW(findFilename.c_str(), &finddata);
    if (findHandle != INVALID_HANDLE_VALUE) {
        do {
            wstring filename = finddata.cFileName;
            if (demosIgnored.find(filename) != demosIgnored.end()) continue;
            if (status == 0)
                StartTime();
            status = 1;
            wstring filepath = settings.get<wstring>(cszSettingGameDir) + filename;
            HANDLE file = CreateFileW(filepath.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);
            if (file == INVALID_HANDLE_VALUE) {
                DWORD error = GetLastError();
                if (error == ERROR_SHARING_VIOLATION) {
                    demosWaitingCount++;
                    demoWaiting = filename;
                }
                continue;
            }
            ProcessDemo(filename, filepath, file);
            CloseHandle(file); // close in case ProcessDemo didn't close
        } while (FindNextFileW(findHandle, &finddata));
    }
    FindClose(findHandle);
    if (status > 0 && demosWaitingCount == 0)
        msLastDemo = GetTimeMillis() + 1000;
}


void SelectGameDir(wstring directory) {
    settings.put(cszSettingGameDir, directory);
    //TODO verify and process
    SetWindowTextW(phGameDir.Path(), directory.length() ? directory.c_str() : L"<no dir>");
}

Game * GetCurrentGame() {
    return game;
}

void LoadSplitsPb(wstring file) {
    if (file.length()) {
        HANDLE handle = CreateFile(file.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
        if (handle == INVALID_HANDLE_VALUE || !LoadSplits<&Map::ticksPb>(handle))
            file = L""; // TODO error message
        CloseHandle(handle);
    } else ClearSplits<&Map::ticksPb>();
    settings.put(cszSettingGamePb, file);
    SetWindowTextW(phSplitsPb.Path(), file.length() ? file.c_str() : L"<no file>");

    UpdateTimeLabels();
}

void LoadSplitsBest(wstring file) {
    if (file.length()) {
        HANDLE handle = CreateFile(file.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
        if (handle == INVALID_HANDLE_VALUE || !LoadSplits<&Map::ticksBest>(handle))
            file = L""; // TODO error message
        CloseHandle(handle);
    } else ClearSplits<&Map::ticksBest>();
    settings.put(cszSettingGameBest, file);
    SetWindowTextW(phSplitsBest.Path(), file.length() ? file.c_str() : L"<no file>");

    UpdateTimeLabels();
}

void Configure() {
    SelectGameDir(settings.get(cszSettingGameDir, wstring(L"C:\\Program Files (x86)\\Steam\\SteamApps\\common\\Portal 2\\portal2\\"))); // TODO auto find dir in Main
    LoadSplitsPb(settings.get(cszSettingGamePb, settings.get<wstring>(cszSettingGameDir) + L"pb.csv"));
    LoadSplitsBest(settings.get(cszSettingGameBest, settings.get<wstring>(cszSettingGameDir) + L"best.csv"));
    SettingsSave(); // TODO error message
}


void UpdateMapLabels(SplitLabel & lbl, LPCSTR name,
                     TicksOpt ticksCurrent, TicksOpt ticksPb, TicksOpt ticksBest,
                     HBRUSH brBackground, bool isChapter = false) {
    lbl.Name().Set(name ? (string("  ") + name).c_str() : "", clWhite, brBackground);

    TicksOpt ticksTarget = ticksPb ? ticksPb : ticksBest;
    if (ticksCurrent) {
        if (ticksTarget) {
            TicksOpt ticksSplit; ticksSplit.addSplit(ticksCurrent, ticksTarget);
            lbl.Time().Set((FormatTime(ticksSplit.time(), 1, true, 1) + "  ").c_str(),
                           !isChapter && ticksBest && ticksCurrent.ticks < ticksBest.ticks ? clGold :
                           ticksSplit.ticks < 0 ? clLime : ticksSplit.ticks == 0 ? clWhite : clRedish, brBackground);
        } else lbl.Time().Set((FormatTime(ticksCurrent.time(), 1, false, 1) + "  ").c_str(), clWhite, brBackground);
    } else if (ticksTarget) {
        lbl.Time().Set((FormatTime(ticksTarget.time(), 1, false, 1) + "  ").c_str(), clWhite, brBackground);
    } else {
        lbl.Time().Set(name ? "---  " : "", clWhite, brBackground);
    }
}

void UpdateTimeLabels() {
    bool hideBest = settings.get<wstring>(cszSettingGamePb).empty();
    TicksOpt ticksCurrent, ticksBest, ticksSplitPb, ticksSplitBest, ticksSumPb, ticksSumBest(0, hideBest);
    for (size_t i = 0; i < game->chaptersCount; i++) {
        Chapter & chapter = (*game)[i];
        for (size_t j = 0; j < chapter.mapsCount; j++) {
            const Map & map = chapter[j];
            ticksCurrent.add(map.ticksCurrent);
            ticksBest.add(map.ticksCurrent ? map.ticksCurrent : map.ticksBest);
            ticksSplitPb.addSplit(map.ticksCurrent, map.ticksPb);
            ticksSplitBest.addSplit(map.ticksCurrent, map.ticksBest);
            ticksSumPb.add(map.ticksPb);
            ticksSumBest.add(map.ticksBest);
        }
        UpdateMapLabels(lblBlock.Chapter(i), chapter.name,
                        ticksCurrent, ticksSumPb, ticksCurrent ? ticksSumBest : ticksBest,
                        i == mapSelected.chapter ? brGray2 : brGray1, true);
    }
    for (size_t i = game->chaptersCount; i < chapterLines; i++)
        UpdateMapLabels(lblBlock.Chapter(i), NULL, 0, 0, 0, brGray1, true);

    if (mapSelected.chapter < game->chaptersCount) {
        Chapter & chapter = (*game)[mapSelected.chapter];
        for (size_t i = 0; i < chapter.mapsCount; i++) {
            const Map &map = chapter[i];
            UpdateMapLabels(lblBlock.Map(i), map.name, map.ticksCurrent, map.ticksPb, TicksOpt(map.ticksBest, hideBest && map.ticksBest != 0),
                            i == mapSelected.map ? brGray3 : brGray2);
        }
    }
    for (size_t i = mapSelected.chapter < game->chaptersCount ? (*game)[mapSelected.chapter].mapsCount : 0; i < chapterLines; i++) {
        UpdateMapLabels(lblBlock.Map(i), NULL, 0, 0, 0, brGray2);
    }

    long long timeEst = 0, timeRta = 0, timeLast = 0, timeLastSplit = 0, timeBest = 0, timeBestSplit = 0;

    ticksCurrent.valid = status != 0;
    switch (status) {
        case 0:
            timeBest = ticksBest.time();
            ticksSplitPb.valid = false;
            ticksSplitBest.valid = false;
            break;

        case 1:
            timeRta = GetTimeMillis() - msRtaEnd;
            timeEst = GetTimeMillis() - msLastDemo;
            [[fallthrough]];
        default:
        case 2:
            timeLast = ticksCurrent.time();
            timeRta += msRtaEnd - msRtaStart;
            timeRta += TicksOpt(ticksAdjStart + ticksAdjEnd, true).time();
            timeEst += timeLast;
            timeLastSplit = ticksSplitPb.time();
            timeBest = ticksBest.time();
            timeBestSplit = ticksSplitBest.time();
            break;
    }

    SetWindowTextA(lblTimeEst, FormatTime(timeEst, 0, false, 3).c_str());
    SetWindowTextA(lblTimeRta, FormatTime(timeRta, 0, false, 3).c_str());
    SetWindowTextA(lblTimeLast, ticksCurrent ? FormatTime(timeLast, 3, false, 2).c_str() : "N/A");
    SetWindowTextA(lblTimeBest, ticksBest ? FormatTime(timeBest, 3, false, 2).c_str() : "N/A");
    lblTimeLastSpl.Set(ticksSplitPb ? FormatTime(timeLastSplit, 3, true, 1).c_str() : "",
                       timeLastSplit < 0 ? clLime : timeLastSplit == 0 ? clWhite : clRedish, brGray0);
    lblTimeBestSpl.Set(ticksSplitBest ? FormatTime(timeBestSplit, 3, true, 1).c_str() : "N/A",
                       timeBestSplit < 0 ? clLime : timeBestSplit == 0 ? clWhite : clRedish, brGray0);
}

void UpdateLabels() {
    UpdateTimeLabels();

    SetWindowTextW(lblNextDemo, demosWaitingCount ? (!DoRename() ? demoWaiting : NextDemoName()).c_str() : L"");

    if (status == 2) {
        lblStatus.Set("Finished!", clDark, brGreen);
    } else {
        if (demosWaitingCount >= 1) {
            wostringstream text; text << demoWaiting;
            if (demosWaitingCount > 1) text << L" (+" << (demosWaitingCount - 1) << " demos)";
            lblStatus.Set(text.str().c_str(), clDark, brBlue);
        } else {
            if (status == 1 && ((GetTimeMillis() / 1000) % 2) == 0)
                lblStatus.Set("Not waiting for any demos!", clRed, brDark);
            else lblStatus.Set("Not waiting for any demos!", clDark, brRed);
        }
    }
}

LRESULT CALLBACK TimerWndProc(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam) {
    switch (umsg) {
        case WM_CREATE: {
            root.hwnd = hwnd;

            for (auto wnd : Window::instances) {
                if (!wnd->Create(hwnd))
                    return -1;
            }

            Configure();
            ResetTime(); // also finds ignored demos
            UpdateLabels();

            SetTimer(hwnd, 1, 250, NULL);

            return DefWindowProc(hwnd, umsg, wparam, lparam);
        }
        case WM_NCDESTROY: {
            root.hwnd = NULL;
            return DefWindowProc(hwnd, umsg, wparam, lparam);
        }
        case WM_CLOSE: {
            HWND notes = GetNotesWindow();
            if (notes) {
                SendMessage(notes, WM_CLOSE, 0, 0); // FIXME receive confirmation
                settings.put(cszSettingNotesOpen, true);
            }

            if (status == 0 ||
                    (!savedRun ?
                (MessageBoxW(root, L"Are you sure you want to exit without saving the current run?",
                             L"Reset Portal 2 Run", MB_ICONWARNING | MB_YESNO) == IDYES) :
                (!savedBest && settings.find(cszSettingGameBest) != settings.not_found()) ?
                (MessageBoxW(root, L"Are you sure you want to update the best splits file?",
                             L"Reset Portal 2 Run", MB_ICONWARNING | MB_YESNO) == IDYES) :
                (MessageBoxW(root, L"Are you sure you want to exit?",
                             L"Reset Portal 2 Run", MB_ICONQUESTION | MB_YESNO) == IDYES))) {

                RECT pos = {};
                if (GetWindowRect(root, &pos)/* && AdjustWindowRect(&pos, (DWORD) GetWindowLongPtr(root, GWL_STYLE), TRUE)*/) {
                    settings.put(cszSettingX, pos.left);
                    settings.put(cszSettingY, pos.top);
                }
                SettingsSave();
                DestroyWindow(root);
            }
            return 0;
        }
        case WM_INITMENU: {
            CheckMenuItem((HMENU) wparam, IDM_TOPMOST, Topmost() ? 8 : 0);
            CheckMenuItem((HMENU) wparam, IDM_RENAME, DoRename() ? 8 : 0);
            return 0;
        }
        case WM_COMMAND: {
            HWND ctl = (HWND) lparam;
            if (lparam == 0) {
                switch (wparam) {
                    case IDM_EXIT:
                        SendMessage(root, WM_CLOSE, 0, 0);
                        return 0;
                    case IDM_NOTES:
                        OpenNotesWindow(NULL, Topmost());
                        return 0;
                    case IDM_TOPMOST: {
                        settings.put(cszSettingTopmost, !Topmost());
                        SetWindowPos(hwnd, Topmost() ? HWND_TOPMOST : HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
                        HWND notes = GetNotesWindow();
                        if (notes)
                            SetWindowPos(notes, Topmost() ? HWND_TOPMOST : HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
                        return 0;
                    }
                    case IDM_RENAME:
                        if (status != 0) {
                            MessageBox(root, L"Can't change DoRename setting while measuring a run. "
                                       "Reset current run to change this setting.", L"Portal 2 Timer by Nulano",
                                       MB_ICONERROR | MB_OK);
                            return 0;
                        }
                        settings.put(cszSettingDoRename, !DoRename());
                        return 0;
                    case IDM_SPEEDRUN_COM:
                        ShellExecuteW(0, 0, L"https://www.speedrun.com/Portal_2", 0, 0, SW_SHOW);
                        return 0;
                    case IDM_HOME:
                        ShellExecuteW(0, 0, L"https://bitbucket.org/nulano/portal2timer", 0, 0, SW_SHOW);
                        return 0;
                    case IDM_ABOUT:
                        // TODO
                        MessageBoxW(root, L"Portal 2 Timer by Nulano version " VERSION_WSTR L"\n"
                                    L"\nUses http://glyphicons.com/ icons under the CC BY 3.0 license",
                                    L"About Portal 2 Timer by Nulano", MB_OK);
                        return 0;
                    default:
                        return 0;
                }
            }
            if (HIWORD(wparam) == STN_CLICKED) {
                for (size_t i = 0; i < game->chaptersCount; i++) {
                    if (ctl == lblBlock.Chapter(i).Name() || ctl == lblBlock.Chapter(i).Time()) {
                        mapSelected = {i, 0};
                        UpdateTimeLabels();
                        ScrollNotes(game->getMap(mapSelected));
                        return 0;
                    }
                }
                for (size_t i = 0; i < (*game)[mapSelected.chapter].mapsCount; i++) {
                    if (ctl == lblBlock.Map(i).Name() || ctl == lblBlock.Map(i).Time()) {
                        mapSelected.map = i;
                        UpdateTimeLabels();
                        ScrollNotes(game->getMap(mapSelected));
                        return 0;
                    }
                }
            }
            if (HIWORD(wparam) == BN_CLICKED) {
                switch (LOWORD(wparam)) {
                    case IDC_GAME_SELECT: { // Select Game Dir
                        wstring dir = settings.get<wstring>(cszSettingGameDir);

                        wchar_t path[4098];
                        wcscpy(path, (dir.substr(0, dir.rfind(L'\\')) + L".exe").c_str());

                        OPENFILENAMEW ofn = {};
                        ofn.lStructSize = sizeof(ofn);
                        ofn.hwndOwner = root;
                        ofn.hInstance = hinstance;
                        ofn.lpstrFilter = L"Portal 2 (portal2.exe)\0"   L"portal2.exe\0"
                                          L"All Files\0"                L"*.*\0";
                        ofn.nFilterIndex = 1;
                        ofn.lpstrFile = path;
                        ofn.nMaxFile = sizeof(path) / sizeof(path[0]);
                        ofn.lpstrInitialDir = NULL; // TODO
                        ofn.lpstrTitle = L"Select Game Executable to select Game Directory";
                        ofn.Flags = OFN_FILEMUSTEXIST;

                        wstring ddir = path;

                        if (GetOpenFileNameW(&ofn))
                            SelectGameDir(ddir.substr(0, ddir.rfind('.')) + L"\\");
                        //TODO else switch(CommDlgExtendedError()) { ... }

                        return 0;
                    }
                    case IDC_PB_OPEN: { // Load PB
                        wchar_t path[4098];
                        wcscpy(path, settings.get<wstring>(cszSettingGamePb, L"").c_str());

                        OPENFILENAMEW ofn = {};
                        ofn.lStructSize = sizeof(ofn);
                        ofn.hwndOwner = root;
                        ofn.hInstance = hinstance;
                        ofn.lpstrFilter = L"Comma Separated Values (*.csv)\0"   L"*.csv\0"
                                          L"All Files\0"                        L"*.*\0";
                        ofn.nFilterIndex = 1;
                        ofn.lpstrFile = path;
                        ofn.nMaxFile = sizeof(path) / sizeof(path[0]);
                        ofn.lpstrInitialDir = NULL; // TODO
                        ofn.lpstrTitle = L"Open Target Run Splits";
                        ofn.Flags = OFN_FILEMUSTEXIST;

                        if (GetOpenFileNameW(&ofn))
                            LoadSplitsPb(path);
                        //TODO else switch(CommDlgExtendedError()) { ... }

                        return 0;
                    }
                    case IDC_PB_CLOSE: { // Unload PB
                        LoadSplitsPb(L"");
                        return 0;
                    }
                    case IDC_BEST_OPEN: { // Load Best
                        wchar_t path[4098];
                        wcscpy(path, settings.get<wstring>(cszSettingGameBest, L"").c_str());

                        OPENFILENAMEW ofn = {};
                        ofn.lStructSize = sizeof(ofn);
                        ofn.hwndOwner = root;
                        ofn.hInstance = hinstance;
                        ofn.lpstrFilter = L"Comma Separated Values (*.csv)\0"   L"*.csv\0"
                                          L"All Files\0"                        L"*.*\0";
                        ofn.nFilterIndex = 1;
                        ofn.lpstrFile = path;
                        ofn.nMaxFile = sizeof(path) / sizeof(path[0]);
                        ofn.lpstrInitialDir = NULL; // TODO
                        ofn.lpstrTitle = L"Open Best Splits";
                        ofn.Flags = OFN_FILEMUSTEXIST;

                        if (GetOpenFileNameW(&ofn))
                            LoadSplitsBest(path);
                        //TODO else switch(CommDlgExtendedError()) { ... }

                        return 0;
                    }
                    case IDC_BEST_CLOSE: { // Unload Best
                        LoadSplitsBest(L"");
                        return 0;
                    }
                    case IDC_BEST_SAVE: { // Save Best
                        if (status == 0) return 0; // TODO disable button

                        wchar_t path[4098];
                        wcscpy(path, settings.get<wstring>(cszSettingGameBest, L"").c_str());

                        OPENFILENAMEW ofn = {};
                        ofn.lStructSize = sizeof(ofn);
                        ofn.hwndOwner = root;
                        ofn.hInstance = hinstance;
                        ofn.lpstrFilter = L"Comma Separated Values (*.csv)\0"   L"*.csv\0"
                                          L"All Files\0"                        L"*.*\0";
                        ofn.nFilterIndex = 1;
                        ofn.lpstrFile = path;
                        ofn.nMaxFile = sizeof(path) / sizeof(path[0]);
                        ofn.lpstrInitialDir = NULL; // TODO
                        ofn.lpstrTitle = L"Save Best Splits";
                        ofn.Flags = OFN_NOREADONLYRETURN | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY; // TODO consider adding overwrite prompt

                        if (GetSaveFileNameW(&ofn)) {
                            wstring file = path;

                            if (!ends_with(file, L".csv") && ofn.nFilterIndex == 1)
                                file.append(L".csv");

                            if (SaveSplits(file, true)) {
                                savedBest = true;
                            } else {
                                wostringstream text;
                                text << L"Error saving best splits!";
                                DWORD code = GetLastError();
                                if (code) {
                                    LPWSTR format = 0;
                                    DWORD formatLen = FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER |
                                                                     FORMAT_MESSAGE_FROM_SYSTEM |
                                                                     FORMAT_MESSAGE_MAX_WIDTH_MASK,
                                                                     NULL, code, 0, (LPWSTR) &format, 128, NULL);
                                    if (formatLen) text << endl << format;
                                    if (format) LocalFree(format);
                                }
                                MessageBoxW(root, text.str().c_str(), L"Error saving best Portal 2 splits", MB_ICONERROR | MB_OK);
                            }
                        } //TODO else switch(CommDlgExtendedError()) { ... }

                        return 0;
                    }
                    case IDC_CURRENT_SAVE: { // Save PB (current run)
                        if (status == 0) return 0; // TODO disable button

                        wchar_t path[4098];
                        wcscpy(path, (settings.get<wstring>(cszSettingGameDir) + attempt + L".csv").c_str());

                        OPENFILENAMEW ofn = {};
                        ofn.lStructSize = sizeof(ofn);
                        ofn.hwndOwner = root;
                        ofn.hInstance = hinstance;
                        ofn.lpstrFilter = L"Comma Separated Values (*.csv)\0"   L"*.csv\0"
                                          L"All Files\0"                        L"*.*\0";
                        ofn.nFilterIndex = 1;
                        ofn.lpstrFile = path;
                        ofn.nMaxFile = sizeof(path) / sizeof(path[0]);
                        ofn.lpstrInitialDir = NULL; // TODO
                        ofn.lpstrTitle = L"Save Current Run Splits";
                        ofn.Flags = OFN_NOREADONLYRETURN | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY;

                        if (GetSaveFileNameW(&ofn)) {
                            wstring file = path;

                            if (!ends_with(file, L".csv") && ofn.nFilterIndex == 1)
                                file.append(L".csv");

                            if (SaveSplits(file, false)) {
                                savedRun = true;
                            } else {
                                wostringstream text;
                                text << L"Error saving run splits!";
                                DWORD code = GetLastError();
                                if (code) {
                                    LPWSTR format = 0;
                                    DWORD formatLen = FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER |
                                                                     FORMAT_MESSAGE_FROM_SYSTEM |
                                                                     FORMAT_MESSAGE_MAX_WIDTH_MASK,
                                                                     NULL, code, 0, (LPWSTR) &format, 128, NULL);
                                    if (formatLen) text << endl << format;
                                    if (format) LocalFree(format);
                                }
                                MessageBoxW(root, text.str().c_str(), L"Error saving Portal 2 run splits", MB_ICONERROR | MB_OK);
                            }
                        } //TODO else switch(CommDlgExtendedError()) { ... }

                        return 0;
                    }
                    case IDC_CURRENT_CLOSE: {
                        if (status == 0) return 0;
                        if (!savedRun ?
                            (MessageBoxW(root, L"Are you sure you want to reset the current run without saving?",
                                         L"Reset Portal 2 Run", MB_ICONWARNING | MB_YESNO) == IDYES) :
                            (!savedBest && settings.find(cszSettingGameBest) != settings.not_found()) ?
                            (MessageBoxW(root, L"Are you sure you want to reset the current run without updating best splits?",
                                         L"Reset Portal 2 Run", MB_ICONWARNING | MB_YESNO) == IDYES) :
                            (MessageBoxW(root, L"Are you sure you want to reset the current run?",
                                         L"Reset Portal 2 Run", MB_ICONQUESTION | MB_YESNO) == IDYES))
                            ResetTime();
                        return 0;
                    }
                }
            }
            return 0;
        }
        case WM_TIMER: {
            ProcessDemos();

            UpdateLabels();

            return 0;
        }
        default:
            return RootWndProc(hwnd, umsg, wparam, lparam);
    }
}

ATOM RegisterTimerWindowClass() {
    WNDCLASS wndclass = {};
//    wndclassex.cbSize = sizeof(wndclass);
    wndclass.style = CS_HREDRAW | CS_VREDRAW;
    wndclass.lpfnWndProc = TimerWndProc;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 0;
    wndclass.hInstance = hinstance;
    wndclass.hIcon = LoadIcon(hinstance, MAKEINTRESOURCE(IDI_ICONMAIN));
    //wndclassex.hIconSm = wndclass.hIcon;
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass.hbrBackground = brGray0;
    wndclass.lpszMenuName = MAKEINTRESOURCE(IDM_MENU);
    wndclass.lpszClassName = clpszRootClassName;

    return RegisterClass(&wndclass);
}

// WinMain arguments are redundant: https://stackoverflow.com/a/25250854/1648883
//int WinMain(HINSTANCE hInstance, HINSTANCE hInstanceOld, LPSTR pCmdLine, int nCmdShow) {
int main() {
    SettingsLoad();
    settings.put(cszSettingLastRun, time(0));

    if (!RegisterIconButtonClass(hinstance))
        return 2;

    if (!RegisterTimerWindowClass())
        return 1;

    if (!RegisterNotesWindowClass())
        return 3;

    DWORD dwStyle = WS_OVERLAPPEDWINDOW & ~WS_SIZEBOX & ~WS_MAXIMIZEBOX;

    RECT rect = root;
    AdjustWindowRect(&rect, dwStyle, TRUE);

    // TODO clip window to monitor
    auto x = settings.get(cszSettingX, CW_USEDEFAULT/* + rect.left*/);
    auto y = settings.get(cszSettingY, CW_USEDEFAULT/* + rect.top*/);
    CreateWindowEx(((DWORD) Topmost() * WS_EX_TOPMOST) | WS_EX_COMPOSITED, clpszRootClassName,
                   TEXT("Portal 2 Timer by Nulano"), dwStyle, x/* - rect.left*/, y/* - rect.top*/,
                   rect.right - rect.left, rect.bottom - rect.top, NULL, NULL, hinstance, 0);
    if (!root.hwnd) {
        MessageBox(NULL, TEXT("Failed to create window"), TEXT("Portal 2 Timer by Nulano"), MB_ICONERROR | MB_OK);
        return 1;
    }

    ShowWindow(root, SW_SHOWNORMAL);

    if (settings.get(cszSettingNotesOpen, true))
        OpenNotesWindow(NULL, Topmost());

    MSG msg = {};
    while (root.hwnd && GetMessage(&msg, NULL, 0, 0)) {
        if (IsDialogMessage(root, &msg))
            continue;
        TranslateMessage(&msg); // handled by IsDialogMessage (not true... RichEdit needs this)
        DispatchMessage(&msg);
    }

    return 0;
}
