//
// Created by Nulano on 9. 8. 2018.
//

#ifndef PORTAL2TIMER_GAME_H
#define PORTAL2TIMER_GAME_H

#include "pch.h"
#include "Demo.h"

const int tps = 60;

typedef struct MapId_ {
    size_t chapter, map;
    MapId_() : chapter(1000), map(1000) {};
    MapId_(size_t chapter, size_t map) : chapter(chapter), map(map) {}
    bool operator == (const MapId_ & o) const { return tie(chapter, map) == tie(o.chapter, o.map); }
    bool operator != (const MapId_ & o) const { return tie(chapter, map) != tie(o.chapter, o.map); }
    bool operator <  (const MapId_ & o) const { return tie(chapter, map) <  tie(o.chapter, o.map); }
    bool operator >  (const MapId_ & o) const { return tie(chapter, map) >  tie(o.chapter, o.map); }
    bool operator <= (const MapId_ & o) const { return tie(chapter, map) <= tie(o.chapter, o.map); }
    bool operator >= (const MapId_ & o) const { return tie(chapter, map) >= tie(o.chapter, o.map); }
} MapId;

typedef long long (*DemoAdjuster)(const Demo & demo, long long tickStart, long long tickEnd);

typedef struct Map_ {
    const LPCSTR file, name;

    Map_(LPCSTR file) : file(file), name(file) {};
    Map_(LPCSTR file, LPCSTR name) : file(file), name(name) {};
    Map_(LPCSTR file, LPCSTR name, DemoAdjuster adjust) : file(file), name(name), adjust(adjust) {};

    const DemoAdjuster adjust = NULL;

    uint32_t ticksCurrent = 0;
    uint32_t ticksPb = 0;
    uint32_t ticksBest = 0;
} Map;

typedef struct Chapter_ {
    const LPCSTR name;
    Map * const maps;
    const size_t mapsCount;

    template<size_t N>
    Chapter_(Map (& maps)[N], LPCSTR name) : name(name), maps(maps), mapsCount(N) {};

    Map& operator[] (size_t i) { return maps[i]; }

    uint32_t ticksCurrent() const { return ticks<&Map::ticksCurrent>(); };
    uint32_t ticksPb     () const { return ticks<&Map::ticksPb     >(); };
    uint32_t ticksBest   () const { return ticks<&Map::ticksBest   >(); };
private:
    template <uint32_t Map::*value>
    uint32_t ticks() const {
        bool v = true;
        uint32_t t = 0;
        for (size_t i = 0; i < mapsCount; i++) {
            t += maps[i].*value;
            v &= maps[i].*value != 0;
        }
        return v ? t : 0;
    }
} Chapter;

typedef struct Game_ {
    const LPCSTR dir, name;
    Chapter * const chapters;
    const size_t chaptersCount;

    template<size_t N>
    Game_(Chapter (& chapters)[N], LPCSTR dir, LPCSTR name) : dir(dir), name(name), chapters(chapters), chaptersCount(N) {};

    Chapter& operator[] (size_t i) { return chapters[i]; }

    bool advance(MapId & id);
    Map * getMap(MapId id);
    Map & lastMap() {
        Chapter & lastChapter = chapters[chaptersCount - 1];
        return lastChapter.maps[lastChapter.mapsCount - 1];
    }
    MapId findMap(LPCSTR file, MapId hint = {0, 0});
} Game;

Game& GetGame_Portal2();

Game* GetGame(LPCSTR dir);

#endif //PORTAL2TIMER_GAME_H
