//
// Created by Nulano on 24. 12. 2017.
//

#ifndef PORTAL2TIMER_ICONBUTTON_H
#define PORTAL2TIMER_ICONBUTTON_H

#include "pch.h"

#define IBM_COLOR (WM_USER+1)

#define IBMC_BG 0
#define IBMC_FG_IDLE 1
#define IBMC_FG_FOCUS 2
#define IBMC_FG_HOVER 3

const LPCTSTR CL_ICONBUTTON = _T("IconButton");

ATOM RegisterIconButtonClass(HINSTANCE hinstance);

#endif //PORTAL2TIMER_ICONBUTTON_H
