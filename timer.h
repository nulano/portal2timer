//
// Created by Nulano on 17. 6. 2019.
//

#ifndef PORTAL2TIMER_TIMER_H
#define PORTAL2TIMER_TIMER_H

#include "pch.h"

const LPCWSTR cszSettingLastRun     = L"Portal2Timer.last_run";
const LPCWSTR cszSettingDoRename    = L"Portal2Timer.do_rename";
const LPCWSTR cszSettingTopmost     = L"Portal2Timer.window.topmost";
const LPCWSTR cszSettingX           = L"Portal2Timer.window.x";
const LPCWSTR cszSettingY           = L"Portal2Timer.window.y";
const LPCWSTR cszSettingGameDir     = L"Portal2Timer.games.Portal2.dir";
const LPCWSTR cszSettingGamePb      = L"Portal2Timer.games.Portal2.pb";
const LPCWSTR cszSettingGameBest    = L"Portal2Timer.games.Portal2.best";

Game * GetCurrentGame();

wiptree & SettingsGet();

#endif //PORTAL2TIMER_TIMER_H
