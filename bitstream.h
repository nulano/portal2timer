//
// Created by Nulano on 28. 8. 2019.
//

#ifndef PORTAL2TIMER_BITSTREAM_H
#define PORTAL2TIMER_BITSTREAM_H

#include "pch.h"

class bitstream {
    deque<bool> data;

public:
    bitstream(const vector<uint8_t> & bytes) {
        for (uint8_t byte : bytes)
            for (int bit = 0; bit < 8; bit++)
                data.push_back((byte & (1 << bit)) != 0);
    }

    bitstream& operator>>(bool& b) {
        b = data.front();
        data.pop_front();
        return *this;
    }

    template<size_t N>
    bitstream& operator>>(bitset<N>& bits) {
        for (size_t bit = 0; bit < N; bit++) {
            bits[bit] = data.front();
            data.pop_front();
        }
        return *this;
    }

    template<typename T>
    bitstream& operator>>(optional<T>& opt) {
        T t;
        *this >> t;
        opt = t;
        return *this;
    }

    bitstream& operator>>(uint8_t& i) {
        bitset<8> data;
        *this >> data;
        i = static_cast<uint8_t>(data.to_ulong());
        return *this;
    }

    template<typename T>
    bitstream& operator>>(T& t) {
        uint8_t data[sizeof(T)];
        for (uint8_t & byte : data)
            *this >> byte;
        memcpy(&t, data, sizeof(T));
        return *this;
    }

    template<typename T>
    T get() {
        T t;
        *this >> t;
        return t;
    }
};


#endif //PORTAL2TIMER_BITSTREAM_H
