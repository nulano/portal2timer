//
// Created by Nulano on 19. 12. 2017.
//

#ifndef PORTAL2TIMER_PCH_H
#define PORTAL2TIMER_PCH_H

#undef NDEBUG
#undef DEBUG
#undef _DEBUG

#ifdef DEBUG
#define _DEBUG
#else
#define NDEBUG
#endif

#define UNICODE
#define _UNICODE
#define _WARN_DEFAULTS
#define _WIN32_IE 0x0600
#define NTDDI_VERSION 0x06000000
#include <windows.h>
//#include <windowsx.h>
#include <shlwapi.h>
#include <shlobj.h>
#include <shellapi.h>
#include <commctrl.h>
#include <richedit.h>
//#include <gdiplus.h>
#include <tchar.h>

#include <bits/stdc++.h>
#include <variant>
#undef NULL // stddef.h redefines NULL to __null
#define NULL 0LL

using namespace std;

#define BOOST_USE_WINDOWS_H

#include <boost/algorithm/string/predicate.hpp>

using namespace boost::algorithm;

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

using namespace boost::property_tree;

#endif //PORTAL2TIMER_PCH_H
