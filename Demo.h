//
// Created by Nulano on 20. 12. 2017.
//

// Useful resources:
// - https://developer.valvesoftware.com/wiki/DEM_Format
// - https://github.com/VSES/SourceEngine2007/blob/master/src_main/utils/demoinfo/
// - https://nekzor.github.io/dem
// - https://github.com/NeKzor/SourceDemoParser.Net/blob/master/src/SourceDemoParser.Net/Frames/UserCmdFrame.cs
// - https://github.com/ValveSoftware/csgo-demoinfo/blob/master/demoinfogo/

#ifndef PORTAL2TIMER_DEMO_H
#define PORTAL2TIMER_DEMO_H

#include "pch.h"

enum DemoMessageType : uint8_t {
    dem_placeholder = 0, // used to initialize DemoMessage variant
    dem_signon = 1,
    dem_packet = 2,
    dem_synctick = 3,
    dem_consolecmd = 4,
    dem_usercmd = 5,
    dem_datatables = 6,
    dem_stop = 7,
    dem_customdata = 8,
    dem_stringtables = 9,
};

constexpr LPCSTR DemoMessageTypeNames[] = {
        "<none>", "SignOn", "Packet", "SyncTick", "ConsoleCmd",
        "UserCmd", "DataTables", "Stop", "CustomData", "StringTables"
};

struct Vector {
    float x, y, z;
};
struct QAngle {
    float x, y, z;
};

struct DemoCmdInfoPlayer {
    uint32_t flags;
    Vector viewOrigin;
    QAngle viewAngles;
    QAngle localViewAngles;
    Vector viewOrigin2;
    QAngle viewAngles2;
    QAngle localViewAngles2;
};

struct DemoCmdInfo {
    DemoCmdInfoPlayer player1, player2;
};

struct DemoPacket {
    DemoCmdInfo info;
    int32_t sequenceIn, sequenceOut;
    vector<uint8_t> data;
};

struct DemoSignOn {
    DemoPacket packet;
};

struct DemoSyncTick {};

struct DemoConsoleCmd {
    string command;
};

struct DemoUserCmd {
    int32_t sequence;

    optional<int32_t> sequenceOpt;
    optional<int32_t> tickCount;
    optional<float> viewAngleX;
    optional<float> viewAngleY;
    optional<float> viewAngleZ;
    optional<float> moveForward;
    optional<float> moveSide;
    optional<float> moveUp;
    optional<bitset<32>> buttons;
    optional<uint8_t> impulse;
    // TODO weaponSelect;
    optional<int16_t> mouseDx;
    optional<int16_t> mouseDy;
    // TODO entityGroundContact
};

struct DemoDataTables {
    // TODO
    vector<uint8_t> data;
};

struct DemoStop {};

struct DemoCustomData {
    uint32_t unknown;
    vector<uint8_t> data;
};

struct DemoStringTables {
    struct Table {
        map<string, vector<uint8_t>> entries;
        map<string, vector<uint8_t>> clientEntries;
    };
    map<string, Table> tables;
};

struct DemoMessage {
    DemoMessageType type;
    int32_t tick;
    uint8_t playerSlot;
    variant<std::monostate, DemoSignOn, DemoPacket, DemoSyncTick,
            DemoConsoleCmd, DemoUserCmd, DemoDataTables, DemoStop,
            DemoCustomData, DemoStringTables> data;
};

typedef struct Demo_ {
    char Magic[8];
    struct {
        uint32_t DemoProtocol;
        uint32_t NetworkProtocol;
        char ServerName[260];
        char ClientName[260];
        char MapName[260];
        char GameDirectory[260];
        float PlaybackTime; // PlaybackTime = Ticks / 60.0 ; TODO float32_t
        int32_t Ticks;
        uint32_t Frames;
        uint32_t SignOnLength;
    } header;
    vector<DemoMessage> messages;
//    explicit Demo_(HANDLE fileHandle);
} Demo;

bool LoadDemo(Demo& demo, const HANDLE & file);

#endif //PORTAL2TIMER_DEMO_H
