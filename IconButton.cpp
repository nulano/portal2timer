//
// Created by Nulano on 24. 12. 2017.
//

#include "pch.h"
#include "IconButton.h"

HINSTANCE hinstance1 = NULL;

LRESULT CALLBACK IconButtonProc(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam) {
    switch (umsg) {
        case WM_CREATE:
            return DefWindowProc(hwnd, umsg, wparam, lparam);
        case WM_DESTROY:
            return DefWindowProc(hwnd, umsg, wparam, lparam);
        case BM_SETIMAGE: {
            HICON icon = (HICON) (INT_PTR) GetWindowLong(hwnd, 0);
            SetWindowLong(hwnd, 0, (LONG) (INT_PTR) lparam);
            RedrawWindow(hwnd, NULL, NULL, RDW_INVALIDATE);
            return (INT_PTR) icon;
        }
        case WM_LBUTTONDOWN:
            SendMessage(GetParent(hwnd), WM_COMMAND, MAKEWPARAM(GetDlgCtrlID(hwnd), BN_CLICKED), (LONG_PTR) hwnd);
            return 0;
        case WM_MOUSEMOVE: {
//            if (!GetWindowLong(hwnd, 4)) {
            TRACKMOUSEEVENT trackmouseevent = {};
            trackmouseevent.cbSize = sizeof(trackmouseevent);
            trackmouseevent.dwFlags = TME_HOVER | TME_LEAVE;
            trackmouseevent.hwndTrack = hwnd;
            trackmouseevent.dwHoverTime = 100;
            TrackMouseEvent(&trackmouseevent);
            RedrawWindow(hwnd, NULL, NULL, RDW_INVALIDATE);
//                SetWindowLong(hwnd, 4, 1);
//            }
            return DefWindowProc(hwnd, umsg, wparam, lparam);
        }
        case WM_SETFOCUS:
        case WM_KILLFOCUS:
            RedrawWindow(hwnd, NULL, NULL, RDW_INVALIDATE);
            return DefWindowProc(hwnd, umsg, wparam, lparam);
        case WM_MOUSELEAVE: {
//            KillTimer(hwnd, 0);
            RedrawWindow(hwnd, NULL, NULL, RDW_INVALIDATE);
            return DefWindowProc(hwnd, umsg, wparam, lparam);
        }
        case WM_MOUSEHOVER: {
            HWND hwndTip = CreateWindow(TOOLTIPS_CLASS, NULL, WS_POPUP | TTS_NOPREFIX | TTS_ALWAYSTIP,
                         CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, hwnd, NULL, hinstance1, 0);
//            SetWindowPos(hwndTip, HWND_TOPMOST,0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
            TCHAR data[4096];
            GetWindowText(hwnd, data, 4096);
            TOOLINFO toolinfo = {};
            toolinfo.cbSize = sizeof(toolinfo);
            toolinfo.hwnd = GetParent(hwnd);
            toolinfo.uFlags = TTF_IDISHWND | TTF_SUBCLASS;
            toolinfo.uId = (UINT_PTR) hwnd;
            toolinfo.lpszText = data;
            SendMessage(hwndTip, TTM_ADDTOOL, 0, (LPARAM) &toolinfo);
            return 0;
        }
        case WM_PAINT: {
            HWND parent = GetParent(hwnd);
            HPEN pen = CreatePen(PS_NULL, 0, 0);
            POINT mouse = {}; GetCursorPos(&mouse);
            RECT rect = {}; GetClientRect(hwnd, &rect);
            RECT windowRect = {}; GetWindowRect(hwnd, &windowRect);

            PAINTSTRUCT paintstruct = {};
            BeginPaint(hwnd, &paintstruct);
            HDC hdc = paintstruct.hdc;

            bool hover = WindowFromPoint(mouse) == hwnd;
            bool focus = GetFocus() == hwnd;

            SaveDC(hdc);
            SelectObject(hdc, pen);
            SelectObject(hdc, (HBRUSH) SendMessage(parent, IBM_COLOR, IBMC_BG, (INT_PTR) hwnd));
            Rectangle(hdc, rect.left, rect.top, rect.right+1, rect.bottom+1);
            SelectObject(hdc, (HBRUSH) SendMessage(parent, IBM_COLOR, hover ? IBMC_FG_HOVER : focus ? IBMC_FG_FOCUS : IBMC_FG_IDLE, (INT_PTR) hwnd));
            RoundRect(hdc, rect.left+1, rect.top+1, rect.right, rect.bottom, 4, 4);
            DrawIconEx(hdc, rect.left+4, rect.top+4, (HICON) (INT_PTR) GetWindowLong(hwnd, 0), (rect.right-rect.left)-8, (rect.bottom-rect.top)-8, 0, NULL, DI_NORMAL);
            RestoreDC(hdc, -1);

            EndPaint(hwnd, &paintstruct);

            DeleteObject(pen);

            return 0;
        }
        default:
            return DefWindowProc(hwnd, umsg, wparam, lparam);
    }
}

ATOM RegisterIconButtonClass(HINSTANCE hInstance) {
    hinstance1 = hInstance;

    WNDCLASS wndclass = {};
//    wndclass.cbSize = sizeof(wndclass);
    wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_PARENTDC;
    wndclass.lpfnWndProc = IconButtonProc;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 4;
    wndclass.hInstance = hinstance1;
    wndclass.hIcon = 0;
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass.hbrBackground = 0;
    wndclass.lpszMenuName = 0;
    wndclass.lpszClassName = CL_ICONBUTTON;

    ATOM atom = RegisterClass(&wndclass);

    return atom;
}
