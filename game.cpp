//
// Created by Nulano on 9. 8. 2018.
//

#include "pch.h"
#include "game.h"

Map * Game_::getMap(MapId id) {
    if (id.chapter < chaptersCount && id.map < chapters[id.chapter].mapsCount)
        return &chapters[id.chapter][id.map];
    return nullptr;
}

bool Game_::advance(MapId &id) {
    if (id.chapter < chaptersCount && id.map < chapters[id.chapter].mapsCount) {
        id.map++;
        if (id.map < chapters[id.chapter].mapsCount)
            return true;
        else {
            id.chapter++; id.map = 0;
            if (id.chapter < chaptersCount)
                return true;
        }
    }
    id = {0, 0};
    return false;
}

MapId Game_::findMap(LPCSTR file, MapId hint) {
    MapId id = hint;
    do {
        if (strcmp(file, getMap(id)->file) == 0)
            return id;
        advance(id);
    } while (id != hint);
    return {};
}

Game* GetGame(LPCSTR dir) {
    if (strcmp("portal2", dir) == 0) return &GetGame_Portal2();
    return nullptr;
}
