# portal2timer

A timer for Portal 2 speedrunning. (TODO more details)

## License

The source code is available under the terms of the Apache License 2.0, available in the file `LICENSE`.

### Icon
The application icon is based on the Portal 2 icon.
It was made by combining the
 `Portal 2\portal2.ico` and
 `Portal 2\portal2\pak01_dir.vpk:materials\hud\stopwatch.vtf`
 image files in Photoshop.
 
### Glyphicons
This application uses icons from [Glyphicons](http://glyphicons.com/) under  the [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/) license.

## Screenshots

![](docs/screenshot1.png)
